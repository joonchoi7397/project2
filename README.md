# Project 2 template #

This is the template for your project 2 submission. For a full description of the Project, please refer to [this document](https://docs.google.com/document/d/1jmFpRdEZJ_Cmk9Hp1q_I7Co9vRo-d5a3fUHcKBYSAPk/edit?usp=sharing).

## Personal information to add

* **Name**: Junyoung Choi
* **Student ID**: 20180693
* **email**: joonchoi518@kaist.ac.kr

## Important notice before submitting

Make sure to create a branch with your studentID number before

---

## Table of content

* **Java_Code** source code (from Project 1): you can modify it
* **Javascript_Code** source code
* Demo **Video** (the actual video, not the link)
* **Description** of projects and notes in README.md (this file). 

## Description of the project

## objectives
---
    This project is to play cannon shooting game. 
    In the game, cannons look like octopus shoot bomb to each other.
 
    First, user can play himself/herself. 
    User can choose the type of the canon. There are several types of cannon that has different power.
    player and computer shoot cannon alternately. 
    If user destroyes every cannon of enemy, 
    user can go to next level. 
    User's data will be saved, so user can load game whenever user wants. 
    The level goes higher, computer becomes smarter and stronger. 
    To beat powerful computer, user can upgrade user's cannon.

    Also, user can play with friend. 
    User and friend shoot cannon alternately until one between them dies. 
    User can choose the type of the cannon and draw the map that they fight themselves.
    There are several kinds of blocks that user can draw.
    Still, users can choose map among 10 different maps instead of drawing themselves. 
    more detailed information of game is informed below.

## How to use
---------------------------------------
### Game Rule
    1. 2 players shoot cannon alternately
    2. Press "UP", "DOWN" arrow to control cannon angle
    3. Press "LEFT", "RIGHT" arrow to control how far cannon to shoot
    4. Press "ENTER" to shoot
    5. default value of hp of cannon is 100 and power of cannon is 10
    6. Game set when every cannon of one of players die
    7. You can check how to play by click "how to play" button
    8. Exit game with "Exit" button

### Object
* #### Cannon : There are 4 types of cannon you can use
    - Citizen : It has nothing special. basic type of cannon.
    ![Alt text](/Javascript_Code/data/image/defaultp1.png) 
    - Knight : It is stronger than others. Its power is greater than 10.
    ![Alt text](/Javascript_Code/data/image/knight.png) 
    - Paladin : It has harder body that others. Its HP is greater than 50.
    ![Alt text](/Javascript_Code/data/image/paladin.png)
    - Pirate : It takes more money when it killed enemy. It earns 12 golds per enemy.
    ![Alt text](/Javascript_Code/data/image/pirate.png)

* #### Block : There are 4 types of blocks consisting the map
    - ![#3d1718](https://placehold.it/15/3d1718/000000?text=+) Basic Block : This is normal block. Your cannon can stand on this basic block.
    - ![#008080](https://placehold.it/15/008080/000000?text=+) Bouncing Block : When your bomb reaches to this bouncing block, it will be bounced.
    - ![#37ee11](https://placehold.it/15/37ee11/000000?text=+) Portal Block : When your bomb reaches to this portal block, it will go to another portal block.
    - ![#6d292c](https://placehold.it/15/6d292c/000000?text=+) Weak Block : You can destroy this weak block with the bomb. Your cannon can't stand here.

### Website
* Account System
    * This game has own account system which can save your personal data how much you play. You can simply sign up in website.
    * If you have account you can load your data from website to application easily and continue playing.
* Connect to Server
    * Server connection with websocket is not already constructed.
    * You can check connection at the bottom of the canvas of website.
    * If it is failed, you can try to connect with "Connect to Server" button easily.
* Load your data
    * If you are logged in, you can click the "Play with Your Data" button.
    * When you click it, you can play game with your own data.
    * More detailed information with playing with your data will be explained below.
* Play in your customized map
    * When you click the "PVP in Your Map" button, you can set for customized pvp game.
    * More detailed information will be explained below.


### You can choose 2 modes : AI, PVP
- AI : fight with computer.
----------  
  - With default account
    * Start new game with "New Game" button in application
    * Load data by "Load Game" button
        - It allows you to play every stage that you have been clear
    * When you clear a stage, you can get gold depending on the number of enemy cannons in the level.( 10 x the number of enemy cannons )
    * You can upgrade your cannon by gold
        * To upgrade power as 5, you need 30 golds
        * To upgrade hp as 10, you need 10 golds
    * When you clear a stage or upgrade cannon, your data will be saved and you can load it by "Load Game"
    * Level 15 is the last level
        * in level 15, you will fight with boss canon which is very strong.
  - With your account
    - Start with "Play with Your Data" in website
    - If you have played with your account, your data will be loaded to application.
    - If not, you should choose your characters among [4 types](#cannon-:-there-are-4-types-of-cannon-you-can-use).
    - With loaded data, play game with "Load Game" button in application in same way explained above. 

- PVP : fight with player 2.
------------------
  - With existed map
    * Start with "PvP" button in application
    * Choose which map to fight among 10 different maps
    * both cannons have default value of status
  - With customized map
    - Start with "PVP in Your Map" in website
    - Choose characters among [types mentioned above](#cannon-:-there-are-4-types-of-cannon-you-can-use) for player 1 and 2 to play
        - you can look around characters by clicking various times while choosing
        - if you click same character twice, it makes you to choose the character
    - Draw map to fight with various kinds of [blocks](#block-:-there-are-4-types-of-blocks-consisting-the-map)
        - description of the blocks is shown right side of the canvas
        - you can choose which block to draw by clicking button on right
        - you can also choose by pressing keyboard 0~4
        - clear the map by pressing c on keyboard
        - click the cell or drag the cell to draw blocks in map
        - if complete drawing, click send button on the bottom of menu list or pressing s on keyboard
    - You can't submit if it is invalid map
        - If 2 cannons cannot be positioned, it is invalid map.
        - While making where to cannon is locating will be shown for you to check if it is valid or not.
    - You can simply go to back step with back button to change your character

## Structure
    This is overall structure of code. If you want to see detail information of code, there are more information below.
![Alt text](/Javascript_Code/data/structure.png)

- Class in java code
    - [Game](#6-game)
    - [Player](#3-player)
    - [Tank](#2-tank)
    - [Bullet](#1-bullet)
    - [Button](#5-button)
    - [Map](#4-map)
- Global variables and functions in java code
    - [Global](#7-global)
- Javascript code
    - [Database](#database)
    - [Variables](#variable)
    - [Functions](#function)
  - If you are not interested in detailed, you can just go to [dependencies](#dependencies)


## Java
-------
## Class
---------------------------------
### 1. Bullet
    Class for bomb that cannon shoot
  * Variable
    * i : float
        * variables to compute the position of the bomb
        * used as a time variable
    - y2, y1, x1 : float
        * variables to compute the position of the bomb
        * y2, y1 are coefficient to compute y position
        * x1 is coefficient to compute x position
    - ix, iy : float
        * initial position of the bomb
        * ix is x position, iy is y position
    - bulletImg : PImage
        * image of the bullet
        * ![Alt text](/Java_Code/Project1_Code/data/bullet.png)
    - size : int
        * size of the bullet
        * normally 35, but boss Cannon has bigger one
    - in : boolean
        * whether the bomb is in the cannon or not
        * when shooting, it becomes true to false
        * if "in" variable toggles twice, cannon is hit by bomb
    - togcnt : int
        * variable to check bomb is hit or not
        * if it becomes 2 then bomb is hit
    - hitId : int
        * Id of the cannon that the bomb is hit
    - power : int
        * power of the bullet
        * give "power" damage to cannon that it hits
    * inBlock : int
        * variable to check which block the bomb is located
        * it is for bomb passing portal block
    * bounceDir : int
        * variable to check direction of the bomb bouncing
        * 1 for vertical and 2 for horizontal
  * Method
    - Bullet : constructor
        * set "power", "ix", "iy" as input
        * compute "x1" and "y1" by using input and trigonometrical function
        * set "y2" by 100 which is acceleration coefficient
        * load bomb image
        * set size of the bomb
        * initialize "togcnt" and "hitId"
        * run the thread
    - void run()
        * increase i by 0.025 per 10ms to make bomb is moving
    - int getHitId()
        * get id of the cannon that the bomb hit
        * return the "hitId"
    - void setHitId(int i)
        * set "hitId" to "i"
    - boolean inIn()
        * return whether bomb is in the cannon or not
    - void setIn(boolean b)
        * set "in" whether bomb is in the cannon or not by "b"
        * when "in" toggle, it increase togcnt by 1
    - boolean isHit()
        * return true if the bomb is hit with cannon
        * which means "togcnt" becomes 2
    - float getX()
        * return the x position of the bomb computing by ix, x1
        * x1*i + ix
    - float getY()
        * return the y position of the bomb computing by iy, y1, y2
        * y2*i^2 + y1*i + iy
    - int getPower()
        * return the power of the bomb
    - boolean draw()
        * draw the bomb on the current position
        * if bullet go out of the map return true
    * void warpB(float tx, float ty)
        * change the position of the bomb according to tx and ty
        * "tx" is for x distance and "ty" is for y distance
    * int getBlock()
        * return which block the bomb is located in
    * boolean stillBlock(int bn)
        * to check the bomb is loacted in the same block before
        * if it is return true
    * int getBounce()
        * to check the bomb is bouncing or not
        * return the direction of bomb bouncing
    * void bounceB(int dir)
        * make bomb bouncing according to "dir"
        * if "dir" is 2, bounce in horizontal way.
        * if it is 1, bounce in vertical way

#### 1 - a. BulletBuilder
    builder to make class of bomb(Bullet) which needs a lot of parameter
* Variable
    * input parameters for Bullet class parameter
    * power : int
    * xpos : float
    * ypos : float
    * dist : float
    * angle : float
* Method
    * to set value of parameter
    * BulletBuilder power(int power)
    * BulletBuilder xpos(float xpos)
    * BulletBuilder ypos(float ypos)
    * BulletBuilder dist(float dist)
    * BulletBuilder angle(float angle)

    * builder to create each default bomb and bomb of boss
    * Bullet createBullet()
    * Bullet createBossBullet()
----

### 2. Tank

    Class for each cannon that shoot bomb
  * Variable
    - power : int
        * amount the power of Cannon
        * bomb it shoot give "power" damages to target
    - hp : int
        * current hp of the cannon
        * cannon dies if it goes 0
    - maxhp : int
        * max hp of the cannon
        * when game start, evey cannon has "maxhp" hp
    - canonImg : PImage
        * image to draw cannon
        * body of the cannon
        * seperate with image of legs for rotating
        * different image depending on the direction of cannon heading and type of the player
        ![Alt text](/Java_Code/Project1_Code/data/canonp1.png) 
        ![Alt text](/Java_Code/Project1_Code/data/canonp2.png)
        ![Alt text](/Java_Code/Project1_Code/data/knight1body.png)
        ![Alt text](/Java_Code/Project1_Code/data/paladin1body.png)
        ![Alt text](/Java_Code/Project1_Code/data/pirate1body.png)
    - WheelImg : PImage
        * image to draw cannon
        * legs of the cannon
        * different image depending on the direction of cannon heading and type of the player
        ![Alt text](/Java_Code/Project1_Code/data/wheelp1.png)
        ![Alt text](/Java_Code/Project1_Code/data/wheelp2.png)
        ![Alt text](/Java_Code/Project1_Code/data/knight1wheel.png)
        ![Alt text](/Java_Code/Project1_Code/data/paladin1wheel.png)
        ![Alt text](/Java_Code/Project1_Code/data/pirate1wheel.png)
    - xpos : int
        * to check the position where cannon is located on x axis
        * compute by coordinate system in the game
    - ypos : int
        * to check the position where cannon is located on y axis
        * compute by coordinate system in the game
    - size : int
        * size of the cannon
        * normally 150 but boss tank has 750
    - dir : int
        * direction of the cannon heading
        * 1 for heading left, -1 for heading right
    - shooting : boolean
        * to check if it is turn for this cannon to shoot
    - shootcomplete : boolean
        * to check whether cannons shoot or not in cannon's turn 
    - canonID : int
        * to classify each cannon in the game
        * usually used to check turn
    - angle : float
        * angle that cannon is heading to
    - distance : distance
        * amount of how far cannon is trying to shoot
    - alive : boolean
        * whether the cannon is die or not
        * if cannon dies, it becomes false
* Method
    - Tank : constructor
        * set "power", "maxhp", "size" as input or default value
        * load images of cannon and legs
        * set "xpos" and "ypos" as 0
        * set direction to left
        * set "shooting" and "shootcomplete" to false
        * initialize "angle" and "distance"
        * set "alive" is true
    - void reinit()
        * re-initialize the state of the cannon
        * set "hp" as "maxhp"
        * initialize "angle", "distance", "ypo", "alive", "shooting", "shootcomplete"
    - void upgradePower(int deg)
        * increase power of the cannon
        * increase "power" by "deg"
    - int getPower()
        * get power of the cannon
        * return "power"
    - void upgradeHP(int deg)
        * increase max hp of the cannon
        * increase "maxhp" by "deg"
    - int getHP()
        * get max hp of the cannon
        * return "maxhp"
    - void setID(int i)
        * set id of the cannon
        * set "canonID" as "i"
    - int getID()
        * get id of the cannon
        * return "canonID"
    - void togShooting()
        * toggle cannon is shooting or not
        * if it was shooting, initialize "angle" and "distance"
        * change "shooting" to different with before
        * set "shootcomplete" as false
    - float getAngle()
        * get angle of the cannon
        * return "angle"
    - float getDist()
        * get how far cannon to shoot
        * return "distance"
    - void incAngle()
        * increase angle of the cannon
        * increase "angle" by 1
    - void decAngle()
        * decrease angle of the cannon
        * decrease "angle" by 1
    - void incDist()
        * increase how far cannon to shoot
        * increase "distance" by 1
    - void decDist()
        * decrease how far cannon to shoot
        * decrease "distance" by 1
    - void setPos(int x, float y)
        * set position "xpos" and "ypos" as "x" and "y" of the game coordinate
    - hurt(int x)
        * get damage by "x" : decrease "hp" by "x"
        * if "hp" go below 0, "alive" becomes false
    - boolean isDie()
        * get whether the cannon is alive or not
        * return "alive"
    - int getX()
        * get x position of the game coordinate
        * return "xpos"
    - float getY()
        * get y position of the game coordinate
        * return "ypos"
    - Bullet shoot()
        * shoot the bomb
        * make the shooting sound
        * return the bomb that cannon shoot
    - void draw()
        * draw the cannon
        * show left hp of the cannon
        * if the cannon's turn arrow to show where to shoot and triangle to show whose turn will also draw 
    - void setP1Tank()
        * set player 1 cannon
        * load images for p1 and set direction as -1
    - void setP2Tank()
        * set player 2 cannon
        * load images for p2 and set direction as 1
    * void setTankType(String type)
        * set type of the tank according to "type"
        * load images for different type of tank


#### 2 - 1. AITank extends Tank

    Class for cannon of AI. It should shoot bomb automatically when its turn.
* Variable
    - ta, td : float
        * variables for computed angle and how far to shoot
        * cannon will shoot with these values
    - bullet : Bullet
        * bomb that the cannon shoot
        * create by shoot() method
    - up : boolean
        * if "ta" is bigger than 0
        * check "angle" should be larger or smaller
    - far : boolean 
        * if "td" is bigger than 500
        * check "distnace" should be larger or smaller
    - updateBullet : boolean
        * if cannon shoot the bomb or not
        * prevent to call shoo() twice
* Method
    - AITank() : constructor
        * same with parent
        * initiallize "bullet" as null
        * initiallize "up", "far" as true
        * initiallize "updateBullet" as false
        * start thread itself
        * sometimes get "power", "maxhp", and "size" as parent
    - void setShoot(boolean up, boolean far)
        * set "up" and "far" value
        * set them as input which is computed from outside of the class
    - void setTar(float ta, float td)
        * set "ta" and "td" value
        * set them as input which is computed from outside of the class
    - boolean canUpdate()
        * to check the cannon shoot bomb or not
        * return "updateBullet"
    - Bullet getbullet()
        * get bomb that the cannon shoot
        * when it is first called, update "updateBullet" as true
    - void run()
        * make the cannon change the angle and how far to shoot continuously
        * it changes "angle" and "distance" to "ta" and "td" per 50ms
    - void draw()
        * almost same with parent
        * if its turn end, initillize "bullet" and "updateBullet"


#### 2 - 1 - 1. BossTank extends AITank

    Cannon for level 15 which has size 750 and huge bomb
* Variable
    - nothing is added
* Method
    - BossTank : constructor
        * make cannon "power" and "maxhp" to 1000, and "size" to 750
    - Bullet shoot()
        * all same with parent
        * just make bigger bomb which size is 150
    - void incDist()
        * because it is bigger, it increase "distance" by 25
    - void draw()
        * change some value to draw red triangle and hp bar visible for its big size
---
### 3. Player

    Class for each player to fight
* Variable
    - num : int
        * player number
        * 1 for p1, 2 for p2
    - canonAmount : int
        * how many cannons player has
        * 1 for human player
    - tankList : ArrayList(Tank)
        * list of cannon that player has
    - money : int
        * how much gold player has
    - level : int
        * which level that player clear at most
    * type : String
        * which type of cannon player has
* Method
    - Player() : constructor
        * set "canonAmount" as 1
        * set tankList as new ArrayList and add new cannon
        * initiallize "money" and "level" as 0 and 0
        * set "money" and "level" as input value
    - void draw()
        * draw the cannon in the "tankList"
    * void setType(String type)
        * set type of cannon player has to "type"
    - void setP(int num)
        * set p1 or p2
        * set "num" as input and cannon for direction of the player
        * set cannon's type according to user's type
    - void reinit()
        * initialize cannon as start
        * call "reinit()" of all cannon
    - void setLevel(int level)
        * set "level" as input
    - int getLevel()
        * get "level"
    - void incMoney(int deg)
        * increase "money" by "deg"
        * if the player's type is "pirate", increase 20% more
    - int getMoney()
        * get "money"
    - void upgradeTank(String type)
        * upgrade cannon max hp or power by money
        * if "type" is "HP", decrease "money" by 10 and increase "maxhp" of cannon by 10
        * if "type" is "Power", decrease "money" by 30 and increase "Power" of cannon by 5
    - Tank getTankbyId(int i)
        * return cannon that has id "i"
        * if player doesn't have, return null
    - boolean checkDie()
        * check whether the player dies
        * if every cannon dies, return true


#### 3 - 1. AIplayer extends Player

    Class for computer player
 * Variable
    - accuracy : int
        * if computer player shoot perfectly, it is hard for player to win.
        * value of "accuracy" is between 0 and 100 so it is possibility for computer to shoot properly
        * it depends on AI level
    - targetX, targetY : int
        * x, y position of the target which is the position of the p1 cannon
* Method 
    - AIplayer() : constructor
        * set empty list of cannon
        * initialize "level" and "accuracy" as 1 and 100
        * set AIplayer to p2
    - void setLevel(int level)
        * with input "level", set cannon of the AI player
        * the number of cannons is 1 (lev 1~4), 2(5~9), 3(10~14)
        * if level is more than 10, some cannon may be powerful or robust
    - void setTarget(Tank target)
        * set "targetX" and "targetY" by position of the input cannon "target"
        * not game coordinate
    - PVector computeShoot(Tank t)
        * set cannon "t" how to shoot by "targetX" and "targetY"
        * "accuracy" is also applied
        * x value of PVector is angle, and y value of PVector is how far to shoot


#### 3 - a. loadPlayer

    Factory to make player
* Method
    - Player makeP1()
        * return p1 player with default setting
    * Player makePq(String type)
        * return p1 player has type "type"
    - Player makeP2()
        * return p2 player with default setting
    * Player makeP2(String type)
        * return p2 player has type "type"
    - Player loadfromData(int id)
        * return player setting with [data](/Java_Code/Project1_Code/data/players.json)
        * get player data which has "id" id
        * if it is not existed return null
    - AIplayer makeAI(int level, Player tarP)
        * return AI player with level "level"
        * set target of it with "tarP"
    - AIplayer makeBoss(Player tarP)
        * return boss player with level 15
        * has boss cannon and target with "tarP"
---

### 4. Map

    Class for constructing map of the game. It consists of 20 x 10 blocks
* Variable
    - w, h : int
        * w = 20, h = 10 which is max position of block in the map
    - blocks : int[][]
        * array with size 20 x 10
        * values represent which [block](#block-:-there-are-4-types-of-blocks-consisting-the-map) is on it.
        * 0 : no block, 1 : basic block, 2 : bouncing block, 3 : portal block, 4 : weak block
    - objects : boolean[][]
        * array with size 20 x 10
        * true if there is object like cannon
    - c : color
        * color of the block
    * portalList : ArrayList of Integer
        * list for linking portal blocks
        * when the bomb is reached to it, bomb goes to next element of the arraylist
* Method
    - Map() : constructor
        * default : map when stage 1, just ground
        * array as input : map consist with input array, setting "portalList" in horizontal order
        * index as input : in [maps.json](/Java_Code/Project1_Code/data/maps.json) , there are array of which block is located on which position, get map value with index
        * JSONObject as input : JSONObject consist with which block is located on whcih position, get map value with object
    - void initialize()
        * intialize the map variable
        * set array size 20 x 10
        * set "w" and "h"
        * set "c" with color
    - void setBlock(int x, int y)
        * set block on the "x", "y" game coordinate
        * set the value in array true
    - boolean blockExist(int x, int y)
        * whether there is block or not on "x", "y"
        * if it is out of the map return false
        * if it is below the map return true
    - boolean sthExist(int x, int y)
        * whether there is block or object or not on "x", "y"
    - void setObject(int x, int y)
        * set object at position "x", "y"
        * because cannon has 2x2 size in game coordinate, it sets 2x2 size block from "x","y" as true
    - void draw()
        * draw color "c" rectangle on the position where basic block is existed
        * draw other blocks with each [color](#block-:-there-are-4-types-of-blocks-consisting-the-map) that they have
    * PVector getDesPortal(int tx, int ty)
        * with location of the portal block "tx","ty", return the location of the next portal block expressed in PVector
        * it is for the next location of the bomb when it is reached to portal block
    * void reinit()
        * re-initailize the map for restart the game.
        * this process is needed for case that value of weak block goes to -1 when it is destroyed
    * int whichBlockhit(int x, int y)
        * return the block representation number that is in "x", "y"

---
### 5. Button

     Class for buttons, extend to detailed button. When button is clicked, classify by "instanceof" and do different function.
* Variable
    - name : String
        * what to write on the button to know what is the button for
    - xpos, ypos : int
        * position of the button, not game coordinate
    - w, h : int
        * width and height of the button
    - c1, c2 : color
        * color of the button default is "c1" and when hovering, becomes "c2"
    - tSize : int
        * text size of button
* Method
    - Button() : constructor
        * set the values of the button
    - boolean isOver()
        * check mouse is on the button or not
    - boolean buttonClick()
        * check mouse is on the button or not, it is used when button is clicked
    - void draw()
        * draw button with its variable information

#### 5 - 1. newButton extends Button

    Button to make new game

#### 5 - 2. setButton extends Button

    Button to load game from data, has two additional button to increase or decrease level
* Variable
    - setLevel : int
        * which level to start the game
    - maxLevel : int
        * max level that player can load, depending on the [data](/Java_Code/Project1_Code/data/players.json) 
* Method
    - setButton : constructor
        * load data to check set "maxLevel" by "id" of the player
    - boolean canAdd()
        * simillar with isOver() but to check mouse is on the right triangle
    - boolean canSub()
        * simillar with isOver() but to check mouse is on the left triangle
    - boolean buttonClick()
        * additionally, check whether click both triangles 
    - int getLevel()
        * return "setLevel" to start the level
    - void draw()
        * same with other button but has new 2 buttons, to increase or decrease selected level
        * they are triangles located on both side of main button

#### 5 - 3. pvpButton extends Button
     Button to start pvp mode game, it turns to choosing map page

#### 5 - 4. howButton extends Button
     Button to show how to play

#### 5 - 5. exitButton extends Button
     Button to exit game

#### 5 - 6. upgradeHPButton extends Button
     Button to upgrade HP after the game

#### 5 - 7. upgradePowerButton extends Button
     Button to upgrade Power after the game

#### 5 - 8. resumeButton extends Button
     There is a function 'pause', this button makes player resume the game

#### 5 - 9. restartButton extends Button
     Button to restart the game when it end

#### 5 - 10. nextLevelButton extends Button
     Button to go next level after player clears stage 

#### 5 - 11. goHomeButton extends Button
     Button to go to main page

#### 5 - 12. mapButton extends Button
     Button to choose map
* Variable
    - map : JSONObject
    * JSONObject that contains information of position of block of map
    - index : int
    * index of map in [data](/Java_Code/Project1_Code/data/maps.json)
* Method
    - mapButton : constructor
        * set "map" as executed map when the button is clicked and name of button with "i"
    - JSONObjeect getMap()
        * return "map"
    - draw()
        * almost same but draw map preview in the button
---

### 6. Game
    Class to progress game
* Variable
    - p1, p2 : Player
        * 2 players who fight
    - gameMap : Map
        - map where 2 players fight
    - level : int
        - for AI mode, which level player are playing
    - gameMode : mode
        - mode is defined by enum outside as "AI" or "PVP"
        - which mode player are playing
    - btnlist : ArrayList of Button
        - to execute different functions, button in the list helps player to play game
    - turn : int
        - which turn is in if p1's, then 1. If p2's, then 2
    - canonNum : int
        - how many cannons are existed in the game
        - ex) if pvp mode, only 2
    - curCanon : int
        - id of the cannon that is shooting now
    - winner : int
        - which player is winner
        - before game end, it is 0
    - gamesetting : boolean
        - after button for game start is clicked, there is seme animation to prepare by thread
        - this is the value to check if preparing stage or not
    - gamestate : state
        - state is defined by enum outside as "MAIN", "GAMEPLAYING", etc
        - what page the game should show to player
    - htplist : arraylist of PImage
        - images to show player how to play
        - entire 4 pages
        - player can see when they click how to play button
        ![a](/Java_Code/Project1_Code/data/howtoplay1.png)
        ![a](/Java_Code/Project1_Code/data/howtoplay2.png)
        ![a](/Java_Code/Project1_Code/data/howtoplay3.png)
        ![a](/Java_Code/Project1_Code/data/howtoplay4.png)
    - htppage : int
        - represent which image to show in how to play state
    - id : String
        - String that is received from websocket
        - id of the account of current user
* Method
    * Game : constructor
        * load "htplist" by images
        * initialize the game to main page
        * start the thread for game setting
    * void intialize()
        * intialize game page to main page
        * buttons for main page are added to "btnlist"
    * void loadMapButton()
        * load 10 map buttons from [data](/Java_Code/Project1_Code/data/maps.json)
        * these map buttons have json object from the data
    * void restartGame()
        * to restart the game re-initialize p1, p2 and game
    * void nextLevel(int level)
        * start the AI mode game level "level" which is next level from previous game
        * re-initialize p1 and make AI for "level"
    * void loadGame(int id, int level)
        * start the AI mode game level "level" which is selected by clicking load game
        * load p1 by "id" and make AI for "level"
    * void pvpset(JSONObject p1, JSONObject p2, int[][] gmap)
        * start pvp game with received data from websocket
        * set player 1 by "p1" and player 2 by "p2"
        * set game map as "gmap"
    * void newGame(boolean pvp)
        * start new game
        * if "pvp" is true, start pvp mode game
        * if not, start ai mode game and initialize player's state to [data](/Java_Code/Project1_Code/data/players.json)
    * void gameStart()
        * make game start after setting the game
    * void choosingMapbyJson(JSONObject map)
        * make "gamemap" by data of json object "map"
    * void choosingMapbyLevel(int level)
        * make "gamemap" depending on level what player is playing
        * it will be loaded from [maps.json](/Java_Code/Project1_Code/data/maps.json)
    * void settingCanon()
        * set id of the cannon in the game from 0 to increasing order
    * void draw()
        * draw the game depending on state of the game
        
        | state         | what to draw                   |
        | ------------- | ------------------------------ |
        | MAIN          | buttons to start game, see how to play, or exit game                        |
        | GAMEPLAYING   | cannons, map, and game mode. if it is AI mode, player can see status him/herself                                          |
        | CHOOSINGMAP   | buttons that choose which map to play in pvp mode                                 |
        | HOWTOPLAY     | how to play by [images](/Java_Code/Project1_Code/data/howtoplay1.png)                |
        | GAMEOVER      | gameover message and button to restart or go home                               |
        | GAMEWIN       | level clear message and button to go next level or home, and upgrade HP or power of cannon                                           |
        | GAMECLEAR     | game clear message and button to go home and upgrade                                 |
        | PAUSEGAME     | game pause message and button to resume, restart and go home                      |

    * void chkBtnClick()
        * if button is clicked do what should happen according to instance of button
    * void pauseGame()
        * pause game
        * state goes to "PAUSEGAME"
    * void resumeGame()
        * resume game
        * state goes to "GAMEPLAYING"
    * boolean is Playing()
        * to check state is "GAMEPLAYING" or not
    * void drawHowToPlay()
        * draw how to play when state is "HOWTOPLAY"
        * draw by images in "htplist" according to "htppage"
    * Tank getTankbyID(int i)
        * get cannon that has id "i"
        * if it is not existed, return null
    * boolean checkEndGame()
        * to check game ends or not
        * if every cannon of one of players die, set winner, game ends, and reeturn true
    * void initPlayer(int id)
        * when new game of AI mode is started, save the status of player with "id" into [data](Project1_Code/players.json)/Java_Code/
    * void savePlayer(int id)
        * when player clear or upgrade his/her cannon, save the status of player with "id" into [data](/Java_Code/Project1_Code/players.json)
    * void endGame()
        * when game ends, make state to "GAMECLEAR", "GAMEWIN", or "GAMEOVER" depending on mode and level
    * void run()
        * thread for setting game
        * when game setting is started, cannons falls to the ground
    * void changeTurn()
        * change turn whcih cannon to shoot depending on id of the cannon
        * if it is AI turn, compute where to shoot and shoot
    * boolean shouldGone(bullet b)
        * check whether bomb should blow up
        * while checking if bomb goes into portal block, change location
        * if bomb goes to bouncing block, bounce the ball with "checkBounce()" function
        * if bomb goes to weak block, destroy the block
    * boolean checkBounce(float bx, float by, Bullet b)
        * to check bomb "b" locating on "bx","by" should be bounced or not
        * if it has to, bounce "b"
    * int checkHit(bullet b)
        * check whether bomb is hit with cannon or not
        * if hit, return the id of the cannon
        * else, return -1
 
### 7. Global
  * Variable
    * game : Game
        * variable for whole game in the application
    * bullet : Bullet
        * variable for bomb in the game
        * there is only one bomb in every moment in the scene
    * pMaker : loadPlayer
        * factory for player object
    * explode : SoundFile
        * sound for bomb is exploding
    * shot : SoundFile
        * sound for cannon shooting bomb
    * bounce : SoundFile
        * sound for bomb is bouncing
    * warp : SoundFile
        * sound for bomb goes into portal block
    * ws : WebsocketServer
        * variable for websocket server to receive data from javascript 
  * Method
    * void setup()
      * set up the size of the canvas and global variable
    * void draw()
      * draw on the canvas depending on the game state
      * check bomb should be drawn or not
    * void webSocketServerEvent(String msg)
      * receiving the data from websocket, do some action with the data
      * if it was just single json object, set id of the current user depending on it. 
      * if it was new user, add information to the [players.json](/Java_Code/Project1_Code/data/players.json)
      * if it was three json objects, set pvp game by data from website
      * setting map in json object to int array to deliver
    * void mousePressed()
      * check which button is clicked in which mode
    * void keyPressed()
      * to control the cannon for the current turn player

## Javascript
---------------
## Database
### User
  - storing user information
    - idnum : the number of the user. every user has different number
    - id : id of the user. every user has different id
    - password : password to log in
    - name : user's name
    - maxhp : user's initial max hp
    - money : user's initial money which would be 0
    - level : user's initial level which would be 0
    - power : user's initial power
    - type : user's type. initially it would be "none". when user chooses it, it will change to that type
## Variable
### In general
  - mode
    - for which screen to show
    - 0 : main, 1 : customizing pvp mode, 4 : load data to processing
  - wsc
    - to check websocket server is connected or not
    - 0 : not connected, 1 : connected, 2 : try to connect
  - firebaseConfig
    - variable for using firebase database
### For user
  - userMax
    - idnum that new user can get
  - user
    - json object that stored information of the user currently logged in
    - if no one is logged in "idnum" of the variable should be -1
### For customizing pvp
  - tmpMap
    - 20x10 sized array for customized map
  - p1
    - json object of the player 1
    - initially set type to "none"
  - p2
    - json object of the player 2
    - initially set type to "none"
  - blockKind
    - when drawing map, kind of block that would be drawn
    - 1 : basic block, 2 : bouncing block, 3 : portal block, 4 : weak block, 0 : erase
  - stage
    - variable for step to customizing pvp
    - 0 : choosing p1 character, 1 : choosing p2 character, 2 : drawing customed map

### Images & Fonts
  - eraserImg
    - used in customizing map to erase blocks
    ![Alt text](/Javascript_Code/data/image/eraser.png) 
  - saveImg
    - used in customizing map to complete drawing map
    ![Alt text](/Javascript_Code/data/image/save.png)
  - homeImg
    - used in various steps to go to the main screen
    ![Alt text](/Javascript_Code/data/image/home.png) 
  - backImg
    - used in various steps to go to the back step
    ![Alt text](/Javascript_Code/data/image/arrow.png) 
  - pirateImg
    - pirate cannon image for player 1
    ![Alt text](/Javascript_Code/data/image/pirate.png) 
  - pirate2Img
    - pirate cannon image for player 2
    ![Alt text](/Javascript_Code/data/image/piratep2.png) 
  - defaultImg
    - citizen cannon image for player 1
    ![Alt text](/Javascript_Code/data/image/defaultp1.png)
  - default2Img
    - citizen cannon image for player 2
    ![Alt text](/Javascript_Code/data/image/defaultp2.png)
  - knightImg
    - knight cannon image for player 1
    ![Alt text](/Javascript_Code/data/image/knight.png)
  - knight2Img
    - knight cannon image for player 2
    ![Alt text](/Javascript_Code/data/image/knightp2.png)
  - paladinImg
    - paladin cannon image for player 1
    ![Alt text](/Javascript_Code/data/image/paladin.png)
  - paladin2Img
    - paladin cannon image for player 2
    ![Alt text](/Javascript_Code/data/image/paladinp2.png)
  - tfont
    - [font](/Javascript_Code/data/font/MuseoModerno-Regular.ttf) for almost everytime  
  - lfont
    - [font](/Javascript_Code/data/font/MuseoModerno-ExtraLight.ttf) for writing connection issue below

## Function
    Function starting with "draw" is drawing function depending on "mode" or "stage". 
    Function starting with "mouse" is mouse event function also depending on "mode" or "stage"
### In general
  - preload()
    - pre-load images and fonts that are used
  - setup()
    - set up canvas size 800 x 600
    - set font to "tfont"
  - draw()
    - draw canvas depending on "mode" and "stage"
    - using function starting with "draw" explained later
    - show checking connection words at bottom of canvas
  - keyPressed()
    - in the making map stage, you can change "blockKind" or complete making map by key pressing
    - 1 : basic block, 2 : bouncing block, 3 : portal block, 4 : weak block, 0 : eraser
    - c : clear the map, s : complete making map
  - mouseReleased()
    - to check button clicked depending on "mode" and "stage"
    - using function starting with "mouse" explained later
  - mousePressed()
    - it is used when drawing the map
  - mouseDragged()
    - It is also used when drawing the map
  
  - initServer()
    - initialize websocket server ws://localhost:8025/Octopus
    - if success, set "wsc" to 1
    - if failed, set "wsc" to 0
  - initializeMax()
    - initialize "userMax" to possible value that no user has
    - to use "userMax" in sign up situation
  - setLoggedin() 
    - set header of the page as logged in with user's name
  - setLoggedout()
    - set header of the page as logged out with log in input boxes
    - you can try to sign up here
  - trylogin()
    - with input values in input boxes, try to log in
    - check if id is in database and proper password
  - initPage()
    - set header depending on "idnum" of "user" by functions "setLoggedout()" or "setLoggedin()"

  - drawMenu()
    - draw main screen with three buttons
    - "Play with Your Data", "Connect to Server", and "PVP in Your Map"
  - drawHomeBtn()
    - draw Home button and Back button in every "mode" except main screen
  - drawGotoPlay()
    - When message is sending to processing application, it is drawed
    - it informs you to go to processing application
  
  - mouseMenu()
    - check wheter three buttons in main screen are clicked and do action
    - if it is not logged in, you can't play with your data
    - if server is not connected, you can't do something except try connecting to server
  - mouseHomeBtn()
    - go to main screen by changing "mode" to 0
  - mouseBackBtn()
    - go to right before step, mainly main screen
    - if you are customizing PVP, go to one step before

### For customizing pvp
  - initMap()
    - initialize "tmpMap" to 20x10 array all filled with 0, which means empty
  - saveMap()
    - convert "tmpMap" to json object to send by websocket
    - combine player 1, player 2, and map in one string and send to processing application
  - validMap()
    - check wheter the map is possible to play or not
    - if possible, return true. else, return false
  - initPlayer()
    - initialize customizing pvp mode in beginning
    - set each player type to "none"

  - drawMakeMap()
    - draw making map screen on canvas
    - draw 20x10 grid representing map
    - show where each player will be positioned
    - draw buttons to guide you which to draw
  - drawp1p2()
    - draw choosing character screen on canvas
    - Which character that each player choose
    - Show which player is selecting now
  - drawcanonList(i)
    - draw possible character that player can choose
    - depending on "i" different character is drawn in different position
  
  - showDescription()
    - describe about the information of the block type out of a canvas
    - only shown in making map stage

  - mouseDrawMap()
    - fill "tmpMap" by "blockKind" and location of your mouse
  - mouseChooseBlock()
    - choose which block to draw by changing "blockKind"
    - if clicking save button, check if it is valid map and send to processing application
  - mousechoosePCharacter(cp)
    - click character player "cp" to choose
    - change the value in "cp" by what you clicks

### For user
  - initCharacter(choose)
    - depending on "mouseChooseCharacter()" below, setting your charcter as "choose"
    - save your data into firebase database to know whenever you log again
    - send to processing application to be able to play with your data immediately
    
  - drawChooseCharacter()
    - if you haven't played once, draw possible characters you can choose to play AI mode
  - drawEachCandidate(i,j,showImg)
    - draw each character that you can choose depending on "i", "j", "showImg"
    - "i", "j" are for where to draw and "showImg" is for how the character looks like
    
  - mouseChooseCharacter()
    - depending on where you clicked, decide your character

### jquery function
  - interaction with button outside of canvas
  - log in, log out, sign up, register
  - pressing enter to log in

## Dependencies
* java
    * library
        - Sound
        - Websockets
* javascript
    * firebase database

## Additional Information
* To check well implementation easily, I make some accounts as gm
    - id : gmdefault / pw : 0000
         * user's type is citizen with default setting and can load level to 15
    - id : gmknight / pw : 0000
         * user's type is knight with default setting and can load level to 15 
    - id : gmpaladin / pw : 0000
         * user's type is paladin with default setting and can load level to 15 
    - id : gmpriate / pw : 0000
         * user's type is pirate with default setting and can load level to 15 
    - id : gmrich / pw : 0000
         * user's type is default with default setting except golds and can load level to 15
         * It has 500 golds
    - id : gmnone / pw : 0000
         * this account is doing nothing but registering, you can choose character by "Play with Your Data"
    - or you can simply makes new account to check
* I used ["prepros"](https://prepros.io/) to access assets in computer. It makes smoother user experience. Actually, it is ok to use live-server in visual studio code with functionality problem. If you use live-server, javascript part sometimes reloads itself. It does not harm execution of function, but reloading makes user log out which makes user log in again. To avoid it, using ["prepros"](https://prepros.io/) is necessary. If it is ok to log in again at that time, you can just use live-server in VScode. 
 
