class Player {
    int num;
	int canonAmount;
	ArrayList<Tank> tankList;
	int money;
    int level;
    String type;

    Player() {
        this.canonAmount = 1;
		this.tankList = new ArrayList<Tank>();
		tankList.add(new Tank());
		this.money = 0;
        this.level = 0;
        this.type = "default";
    }


	Player(int money,int level) {
        this.canonAmount = 1;
		this.tankList = new ArrayList<Tank>();
		this.money = money;
        this.setP(1);
        this.level = level;
        this.type = "default";
	}

    void draw() {
        for(Tank tank : tankList) {
            tank.draw();
        }
    }

    void setType(String type) {
        this.type = type;
        return;
    }

    void setP(int num) {
        this.num = num;
        if(num == 1) {
            for(Tank tmp : this.tankList) {
                tmp.setP1Tank();
                tmp.setTankType(this.type);
                println("type : " + this.type);
                tmp.setPos(1,0);
            }
        } else {
            for(Tank tmp : this.tankList) {
                tmp.setP2Tank();
                tmp.setTankType(this.type);
                tmp.setPos(17,0);
            }
        }
    }

    void reinit() {
        for(Tank tank : tankList) {
            tank.reinit();
        }
    }

    void setLevel(int level) {
        this.level = level;
    }

    int getLevel() {
        return this.level;
    }

    void incMoney(int deg) {
        if(this.type.equals("pirate")) {
            this.money += deg / 10 * 2;
        }
        this.money += deg;
    }

    int getMoney() {
        return this.money;
    }

    void upgradeTank(String type) {
        if(type.equals("HP")) {          
            if(this.money < 10) {
                return;
            }
            this.money -= 10;
            this.tankList.get(0).upgradeHP(10);
        } else if(type.equals("Power")) {
            if(this.money < 30) {
                return;
            }
            this.money -= 30;    
            this.tankList.get(0).upgradePower(5);
        }
    }

    public Tank getTankbyID(int i) {
        for(Tank t : tankList) {
            if(t.getID() == i) {
                return t;
            }
        }
        return null;
    }

    public boolean checkDie() {
        int cnt = 0;
        for(Tank t : tankList) {
            if(!t.isDie()) {
                return false;
            }
        }
        return true;
    }


}

class AIplayer extends Player {
    int accuracy;
    int targetX, targetY;


    AIplayer() {
        super();
        this.tankList.remove(0);
        this.level = 1;
        this.accuracy = 100;
        this.setP(2);
    }

    void setLevel(int level) {
        this.level = level;
        this.accuracy = 25 + level * 5;
        this.canonAmount = level / 5 + 1;
        while(this.tankList.size() < this.canonAmount) {
            AITank newTank = new AITank(10, 100, 150);
            newTank.setP2Tank();
            if(level > 10) {
                float tmp = random(0,10);
                if(tmp > 8) {
                    newTank.upgradePower(10);
                }
                if(tmp > 6) {
                    newTank.upgradeHP(50);
                }
            }
            this.tankList.add(newTank);
        }
        for(int i = 0; i < this.tankList.size(); i++) {
            tankList.get(i).setPos(17 - 3 * i, 0);
        }
    }

    void setTarget(Tank target) {
        this.targetX = target.getX() * 75 + 75;
        this.targetY = (int)target.getY() * 75 + 80;
    }

    PVector computeShoot(Tank t) {
        float a;
        float d;
        int ix = t.getX() * 75 + 75;
        int iy = (int)t.getY() * 75 + 80;
        float x1, y1;
        float y2 = 100;
        float i;
        float yval;
        for(d = 250; d < 1000; d++) {
            for(a = 20; a < 90; a++) {
                x1 = d * cos(PI / 180 * a) * (-1);
                y1 = d * sin(PI / 180 * a) * (-1);
                i = (this.targetX - ix) / x1;
                yval = iy + y1*i + y2*i*i;
                if(this.targetY - yval < 75 && this.targetY - yval > -75) {
                    return new PVector(a + random(-100 + this.accuracy,100 - this.accuracy)/2,d + random(-100 + this.accuracy,100 - this.accuracy));
                }
            }
        }
        return new PVector(30,500);
    }
}

class loadPlayer {

    Player makeP1() {
        Player res = new Player();
        res.setP(1);
        return res;
    }

    Player makeP1(String type) {
        Player res = new Player(0,0);
        int tmpP = 10;
        int tmpH = 100;
        if(type.equals("knight")) tmpP = 20;
        if(type.equals("paladin")) tmpH = 150;
        Tank t = new Tank(tmpP,tmpH,150);
        res.setType(type);
        res.tankList.add(t);
        res.setP(1);
        return res;
    }

    Player makeP2() {
        Player res = new Player();
        res.setP(2);
        return res;
    }

    Player makeP2(String type) {
        Player res = new Player(0,0);
        int tmpP = 10;
        int tmpH = 100;
        if(type.equals("knight")) tmpP = 20;
        if(type.equals("paladin")) tmpH = 150;
        Tank t = new Tank(tmpP,tmpH,150);
        res.setType(type);
        res.tankList.add(t);
        res.setP(2);
        return res;
    }

    Player loadfromData(String id) {
        File f = new File(sketchPath("data/players.json"));
		if(!f.exists()) return null;

        JSONObject players = loadJSONObject("data/players.json");

        JSONArray list = players.getJSONArray("players");
        if(list == null) {
            return null;
        }

        for(int i = 0; i < list.size(); i++) {
			JSONObject player = list.getJSONObject(i);
            if(player.getString("id").equals(id)) {
                Player p = new Player(player.getInt("money"),player.getInt("level"));
                Tank t = new Tank(player.getInt("power"),player.getInt("maxhp"),150);
                p.setType(player.getString("type"));
                p.tankList.add(t);
                p.setP(1);
                return p;
            }
		}
        return null;
    }

    AIplayer makeAI(int level, Player tarP) {
        if(level == 15) {
            return this.makeBoss(tarP);
        }
        AIplayer res = new AIplayer();
        res.setLevel(level);
        res.setTarget(tarP.tankList.get(0));
        return res;
    }

    AIplayer makeBoss(Player tarP) {
        AIplayer res = new AIplayer();
        res.level = 15;
        BossTank tmp = new BossTank();
        tmp.setP2Tank();
        tmp.setPos(14,0);
        res.tankList.add(tmp);
        res.setTarget(tarP.tankList.get(0));
        return res;
    }
}