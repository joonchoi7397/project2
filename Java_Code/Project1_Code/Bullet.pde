class Bullet extends Thread {
    private float y2, y1;
    private float x1;
    private float ix, iy;
    private float i;
    private PImage bulletImg;
    private int size;
    private boolean in;
    private int togcnt;                         //to check it is hit
    private int hitId;                          //which tank to hit
    private int power;
    private int inBlock;
    private int bounceDir;                      // 0 : none, 1 : up,down, 2 : right,left

    Bullet(int power, float xpos, float ypos, float dist, float angle) {
        this.power = power;
        this.ix = xpos;
        this.iy = ypos;
        this.x1 = dist * cos(PI / 180 * angle) * (-1);
        this.y1 = dist * sin(PI / 180 * angle) * (-1);
        this.y2 = 100;
        this.i = 0;
        this.bulletImg = loadImage("bullet.png");
        this.size = 35;
        this.in = true;
        this.togcnt = 0;
        this.hitId = -1;
        this.inBlock = 0;
        this.bounceDir = 0;
        start();
    }

    Bullet(int power, float xpos, float ypos, float dist, float angle,int size) {
        this.power = power;
        this.ix = xpos;
        this.iy = ypos;
        this.x1 = dist * cos(PI / 180 * angle) * (-1);
        this.y1 = dist * sin(PI / 180 * angle) * (-1);
        this.y2 = 200;
        this.i = 0;
        this.bulletImg = loadImage("bullet.png");
        this.size = size;
        this.in = true;
        this.togcnt = 0;
        this.hitId = -1;
        this.inBlock = 0;
        this.bounceDir = 0;
        start();
    }

    void run() {
        while(true) {
            try {
                Thread.sleep(10);
            }catch (Exception e) {}
            i+=0.025;
        }
    }

    public void warpB(float tx, float ty) {
        ix += 75 * tx;
        iy += 75 * ty;
    }

    public int getHitId() {
        return this.hitId;
    }

    public void setHitId(int i) {
        this.hitId = i;
    }

    public boolean isIn() {
        return this.in;
    }

    public void setIn(boolean b) {
        if(this.isIn() != b) {
            togcnt++;
        }
        this.in = b;
    }

    public boolean isHit() {
        if(this.togcnt == 2) {
            return true;
        }
        return false;
    }

    public int getBlock() {
        return this.inBlock;
    }

    public boolean stillBlock(int bn) {
        if(bn != this.inBlock) {
            this.inBlock = bn;
            return false;
        }
        return true;
    }

    public int getBounce() {
        return this.bounceDir;
    }

    public void bounceB(int dir) {                              //1 : up down, 2 : right left
        this.bounceDir = dir;
        if(dir == 1) {
            this.ix = getX();
            this.iy = getY();
            this.y1 = (y1 + 200 * i) * -1;
            this.i = 0;
            
        } if (dir == 2) {
            this.ix = getX();
            this.iy = getY();
            this.y1 = (y1 + 200 * i);
            this.x1 *= -1;
            this.i = 0;
        }
    }

    private float getX() {
        return ix + x1*i;
    }

    private float getY() {
        return iy + y1*i + y2*i*i;
    }

    int getPower() {
        return this.power;
    }

    boolean draw() {
        imageMode(CENTER);
        image(this.bulletImg, getX(), getY(), size, size);
        if(getX() > 1500 || getX() < 0) {
            return true;
        }
        if((int)getY() > 800) {
            return true;
        }
        return false;
    }


}

class BulletBuilder {
    private int power;
    private float xpos;
    private float ypos;
    private float dist;
    private float angle;

    public BulletBuilder() {}

    public BulletBuilder power(int power) {
        this.power = power;
        return this;
    }

    public BulletBuilder xpos(float xpos) {
        this.xpos = xpos;
        return this;
    }

    public BulletBuilder ypos(float ypos) {
        this.ypos = ypos;
        return this;
    }

    public BulletBuilder dist(float dist) {
        this.dist = dist;
        return this;
    }

    public BulletBuilder angle(float angle) {
        this.angle = angle;
        return this;
    }

    public Bullet createBullet() {
        return new Bullet(power, xpos, ypos, dist, angle);
    }

    public Bullet createBossBullet() {
        return new Bullet(power, xpos, ypos, dist, angle, 150);
    }
}