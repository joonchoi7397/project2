
class Tank{
	protected int power;                                  //power of the bullet
    protected int hp;                                     //hp if goes to 0 die
    protected int maxhp;
    protected PImage canonImg;                            //img
    protected PImage wheelImg;
    protected int xpos;                             //to draw
    int size;
    protected float ypos;
    protected int dir;                                    //heading right : -1 or left : 1
    protected boolean shooting;                           //if it is in shooting procedure
    protected boolean shootcomplete;
    protected int canonID;                                //to check turn
    
    protected float angle;                                //for shooting bullet
    protected float distance;
    protected boolean alive;

	Tank() {
		this.power = 10;
        this.hp = 100;
        this.maxhp = 100;
        this.canonImg = loadImage("canonp1.png");
        this.wheelImg = loadImage("wheelp1.png");
        this.xpos = 0;                                    //xpos ypos size should be changed
        this.ypos = 0;
        this.size = 150;
        this.dir = 1;
        this.shooting = false;
        this.shootcomplete = false;
        this.angle = 0;
        this.distance = 500;
        this.alive = true;
	}

    Tank(int power,int maxhp, int size) {
        this.power = power;
        this.hp = maxhp;
        this.maxhp = maxhp;
        this.canonImg = loadImage("canonp1.png");
        this.wheelImg = loadImage("wheelp1.png");
        this.xpos = 0;
        this.ypos = 0;
        this.size = size;
        this.dir = 1;
        this.shooting = false;
        this.shootcomplete = false;
        this.angle = 0;
        this.distance = 500;
        this.alive = true;
    }

    void reinit() {
        this.shooting = false;
        this.shootcomplete = false;
        this.angle = 0;
        this.distance = 500;
        this.hp = this.maxhp;
        this.ypos = 0;
        this.alive = true;
    }

    void upgradePower(int deg) {
        this.power+=deg;
    }

    int getPower() {
        return this.power;
    }

    void upgradeHP(int deg) {
        this.maxhp+=deg;
        this.hp+=deg;
    }

    int getHP() {
        return this.maxhp;
    }

    void setID(int i) {
        this.canonID = i;
    }

    int getID() {
        return this.canonID;
    }

    void togShooting() {
        if(this.shooting) {
            this.angle = 0;
            this.distance = 500;
        }
        this.shooting = !this.shooting;
        this.shootcomplete = false;
    }

    float getAngle() {
        return this.angle;
    }

    float getDist() {
        return this.distance;
    }

    void incAngle() {
        if(this.angle >= 90) {
            return;
        }
        this.angle++;
    }

    void decAngle() {
        if(this.angle < -45) {
            return;
        }
        this.angle--;
    }

    void incDist() {
        this.distance++;
    }

    void decDist() {
        if(this.distance <= 300) {
            return;
        }
        this.distance--;
    }

    void setPos(int x, float y) {
        if(x < 0 || x > 18) {
            return;
        }
        this.xpos = x;
        this.ypos = y;
    }

    void hurt(int x) {
        this.hp-=x;
        if(this.hp <= 0) {
            this.alive = false;
        }
    }

    boolean isDie() {
        return !this.alive;
    }

    int getX() {
        return this.xpos;
    }

    float getY() {
        return this.ypos;
    }

    Bullet shoot() {
        if(this.shootcomplete) {
            return null;
        }
        this.shootcomplete = true;
        shot.play();
        BulletBuilder bb = new BulletBuilder();
        return bb.power(this.power).xpos((float)xpos*75 + 75).ypos(ypos*75+80).dist(distance*dir).angle(angle*dir).createBullet();
    }

    void draw() {
        if(this.alive) {

            imageMode(CENTER);

            pushMatrix();
            translate(xpos*75 + 75, ypos*75+80);
            rotate(PI / 180 * angle * dir);
            if(this.shooting) {
                stroke(#FF0000);
                strokeWeight(3);
                line(0, 0, (-1) * dir * distance / 4, 0);
                line((-1) * dir * distance / 4, 0, (-1) * dir * distance / 4 - (-1) * dir * 10 * distance / 500, -10 * distance / 500);
                line((-1) * dir * distance / 4, 0, (-1) * dir * distance / 4 - (-1) * dir * 10 * distance / 500, 10 * distance / 500);
                noStroke();
                strokeWeight(1);
            }
            image(this.canonImg, 0, 0, size, size);
            popMatrix();

            image(this.wheelImg, xpos*75 + 75, ypos*75+80, size, size);

            if(this.shooting) {
                fill(#FF0000);
                triangle(xpos*75 + 77, ypos*75 + 35, xpos*75 + 57, ypos*75 + 1, xpos*75 + 97, ypos*75 + 1);
            }
            
            rectMode(CORNER);

            fill(#F0F0F0);
            rect(xpos*75 + 40, ypos*75 + 140,size / 2,5);
            fill(#00FF00);
            rect(xpos*75 + 40, ypos*75 + 140,size / 2 * hp / maxhp,5);

        }

    }

    void setP1Tank() {
        this.canonImg = loadImage("canonp1.png");
        this.wheelImg = loadImage("wheelp1.png");
        this.dir = -1;
    }

    void setP2Tank() {
        this.canonImg = loadImage("canonp2.png");
        this.wheelImg = loadImage("wheelp2.png");
        this.dir = 1;
    }

    void setTankType(String type) {
        if(type.equals("default")) {
            return;
        }
        String p;
        if(this.dir == -1) {
            p = type + "1";
        } else {
            p = type + "2";
        }
        this.canonImg = loadImage(p + "body.png");
        this.wheelImg = loadImage(p + "wheel.png");
    }
}

class AITank extends Tank implements Runnable{
    float ta, td;
    Bullet bullet;
    boolean up, far;
    boolean updateBullet;

    AITank() {
        super();

        this.bullet = null;
        this.up = true;
        this.far = true;
        this.updateBullet = false;
        Thread t = new Thread(this);
        t.start();
	}

    AITank(int power, int maxhp, int size) {
        super(power, maxhp, size);
        this.bullet = null;
        this.up = true;
        this.far = true;
        this.updateBullet = false;
        Thread t = new Thread(this);
        t.start();
    }

    void setShoot(boolean up, boolean far) {
        this.up = up;
        this.far = far;
    }

    void setTar(float ta, float td) {
        this.ta = ta;
        this.td = td;
    }

    boolean canUpdate() {
        return !this.updateBullet;
    }

    Bullet getbullet() {
        if(this.bullet != null) {
            this.updateBullet = true;
        }
        return bullet;
    }

    void run() {
        while(true) {
            try {
                Thread.sleep(50);
            }catch (Exception e) {}
            if(this.shooting && !this.shootcomplete) {
                if(this.up) {
                    if(this.ta > this.angle) {
                        this.incAngle();
                    } else if(this.far && this.td > this.distance) {
                        this.incDist();
                    } else if(!this.far && this.td < this.distance) {
                        this.decDist();
                    } else {
                        this.bullet = this.shoot();
                    }
                } else {
                    if(this.ta < this.angle) {
                        this.decAngle();
                    } else if(this.far && this.td > this.distance) {
                        this.incDist();
                    } else if(!this.far && this.td < this.distance) {
                        this.decDist();
                    } else {
                        this.bullet = this.shoot();
                    }
                }
            }
        }
    }

    void draw() {

        if(this.alive) {

            imageMode(CENTER);
            pushMatrix();
            translate(xpos*75 + 75, ypos*75+80);
            rotate(PI / 180 * angle * dir);
            if(this.shooting) {
                stroke(#FF0000);
                strokeWeight(3);
                line(0, 0, (-1) * dir * distance / 4, 0);
                line((-1) * dir * distance / 4, 0, (-1) * dir * distance / 4 - (-1) * dir * 10 * distance / 500, -10 * distance / 500);
                line((-1) * dir * distance / 4, 0, (-1) * dir * distance / 4 - (-1) * dir * 10 * distance / 500, 10 * distance / 500);
                noStroke();
                strokeWeight(1);
            } else {
                this.bullet = null;
                this.updateBullet = false;
            }
            image(this.canonImg, 0, 0, size, size);
            popMatrix();

            image(this.wheelImg, xpos*75 + 75, ypos*75+80, size, size);

            if(this.shooting) {
                fill(#FF0000);
                triangle(xpos*75 + 77, ypos*75 + 35, xpos*75 + 57, ypos*75 + 1, xpos*75 + 97, ypos*75 + 1);
            }
            
            rectMode(CORNER);
            fill(#F0F0F0);
            rect(xpos*75 + 40, ypos*75 + 140,size / 2,5);
            fill(#00FF00);
            rect(xpos*75 + 40, ypos*75 + 140,size / 2 * hp / maxhp,5);
        
        }
    }
}


class BossTank extends AITank {

    BossTank() {
        super(1000,1000,750);
    }

    Bullet shoot() {
        if(this.shootcomplete) {
            return null;
        }
        this.shootcomplete = true;
        shot.play();
        BulletBuilder bb = new BulletBuilder();
        return bb.power(this.power).xpos((float)xpos*75 + 75).ypos(ypos*75+80).dist(distance*dir).angle(angle*dir).createBossBullet();
    }

    void incDist() {
        this.distance+=25;
    }


    void draw() {
        if(this.alive) {

            imageMode(CENTER);

            pushMatrix();
            translate(xpos*75 + 75, ypos*75+80);
            rotate(PI / 180 * angle * dir);
            if(!this.shooting) {
                this.bullet = null;
                this.updateBullet = false;
            }
            image(this.canonImg, 0, 0, size, size);
            popMatrix();

            image(this.wheelImg, xpos*75 + 75, ypos*75+80, size, size);

            if(this.shooting) {
                fill(#FF0000);
                triangle(xpos*75 + 80, ypos*75 - 111, xpos*75 + 60, ypos*75 - 145, xpos*75 + 100, ypos*75 - 145);
            }
            
            rectMode(CORNER);
            fill(#F0F0F0);
            rect(xpos*75 - 160, ypos*75 + 340,size / 1.5,5);
            fill(#00FF00);
            rect(xpos*75 - 160, ypos*75 + 340,size / 1.5 * hp / 1000,5);
        }
    }
}