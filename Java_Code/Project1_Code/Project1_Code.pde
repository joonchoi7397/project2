/*
	Project 1
	Name of Project: This is not an Octopus
	Author: Junyoung Choi
	Date: 2020.05.31
*/

import websockets.*;
import java.util.*;
import processing.sound.*;


Game game;
Bullet bullet = null;
loadPlayer pMaker = new loadPlayer();
SoundFile explode;
SoundFile shot;
SoundFile bounce;
SoundFile warp;
WebsocketServer ws;

void setup()
{
	// your code
	size(1500,800);
	noStroke();
	game = new Game();
	explode = new SoundFile(this,"explode.wav");
	shot = new SoundFile(this,"shoot.wav");
	bounce = new SoundFile(this,"bounce.wav");
	warp = new SoundFile(this,"warp.mp3");

	ws = new WebsocketServer(this, 8025, "/Octopus");
}

void draw()
{
	// your code
	background(#000269);
	
	if(game.isPlaying()) {
		if(bullet != null) {
			if(bullet.draw()) {
				bullet = null;
				game.changeTurn();
			} else {
				int hitTankId = game.checkHit(bullet);
				int damage = bullet.getPower();
				if(hitTankId != -1) {
					bullet = null;
					explode.play();
					game.getTankbyID(hitTankId).hurt(damage);
					if(!game.checkEndGame()) {
						game.changeTurn();
					}
				} else if(game.shouldGone(bullet)) {
					bullet = null;
					explode.play();
					game.changeTurn();
				}
			}
		} else {
			Tank curCan = game.getTankbyID(game.curCanon);
			if(curCan instanceof AITank) {
				if(((AITank)curCan).canUpdate()) {
					bullet = ((AITank)curCan).getbullet();
				}
			}
		}
	}
	
	rectMode(CORNER);
	game.draw();
	
	
}


// your code down here
// feel free to crate other .pde files to organize your code

void webSocketServerEvent(String msg)
{
	String[] msgL = msg.split("\n");
	if(msgL.length == 1) {
		JSONObject json = parseJSONObject(msg);
		try {
			//synchronized(game){
				game.id = json.getString("id");
				File f = new File(sketchPath("data/players.json"));
				JSONObject players;
				JSONArray list;
				if(!f.exists()) {
					players = new JSONObject();
					list = new JSONArray();
				} else {
					players = loadJSONObject("data/players.json");
					list = players.getJSONArray("players");
					if(list == null) {
						list = new JSONArray();
					}
				}
				if(list.size() == 0) {
					JSONObject player = new JSONObject();
					player.setInt("level",json.getInt("level"));
					player.setInt("power",json.getInt("power"));
					player.setInt("money",json.getInt("money"));
					player.setInt("maxhp",json.getInt("maxhp"));
					player.setString("type",json.getString("type"));
					player.setString("id",json.getString("id"));
					list.append(player);
					players.setJSONArray("players",list);
					saveJSONObject(players,"data/players.json");
					game.initialize();
					return;
				}
				for(int i = 0; i < list.size(); i++) {
					JSONObject player = list.getJSONObject(i);
					if(player.getString("id").equals(json.getString("id"))) {
						game.initialize();
						return;
					}
				}
				JSONObject player = new JSONObject();
				player.setInt("level",json.getInt("level"));
				player.setInt("power",json.getInt("power"));
				player.setInt("money",json.getInt("money"));
				player.setInt("maxhp",json.getInt("maxhp"));
				player.setString("type",json.getString("type"));
				player.setString("id",json.getString("id"));
				list.append(player);
				players.setJSONArray("players",list);
				saveJSONObject(players,"data/players.json");
				game.initialize();
				return;
			//}
		} catch(Exception ie) {
			println("Invalid command");
		}
	} else {
		JSONObject p1 = parseJSONObject(msgL[0]);
		JSONObject p2 = parseJSONObject(msgL[1]);
		JSONObject tmp = parseJSONObject(msgL[2]);
		JSONArray list = tmp.getJSONArray("blocks");
		int[][] gmap = new int[20][10];
		for(int i = 0; i < list.size(); i++) {
			JSONObject tb = list.getJSONObject(i);
            gmap[tb.getInt("x")][tb.getInt("y")] = tb.getInt("type");
		}
		try {
			//synchronized(game){
				game.pvpset(p1, p2, gmap);
			//}
			return;
		} catch(Exception ie) {
			println("Invalid command");
		}
	}
	
}

void mousePressed()
{
	game.chkBtnClick();
}

void keyPressed() {
	if(game.curCanon >= 0 && game.curCanon < game.canonNum) {
		if(game.turn == 1) {
			Tank curCan = game.getTankbyID(game.curCanon);
			if(keyCode == LEFT) {
				curCan.decDist();
			} else if(keyCode == RIGHT) {
				curCan.incDist();
			} else if(keyCode == UP) {
				curCan.incAngle();
			} else if(keyCode == DOWN) {
				curCan.decAngle();
			} else if(keyCode == ENTER) {
				if(bullet == null) {
					bullet = curCan.shoot();
				}
			}
		} else if(game.gameMode == mode.PVP) {
			Tank curCan = game.getTankbyID(game.curCanon);
			if(keyCode == LEFT) {
				curCan.incDist();
			} else if(keyCode == RIGHT) {
				curCan.decDist();
			} else if(keyCode == UP) {
				curCan.incAngle();
			} else if(keyCode == DOWN) {
				curCan.decAngle();
			} else if(keyCode == ENTER) {
				if(bullet == null) {
					bullet = curCan.shoot();
				}
			}
		}	
	}
}

public enum mode {PVP, AI}
public enum state {MAIN, GAMEPLAYING, HOWTOPLAY, GAMEOVER, CHOOSINGMAP, PAUSEGAME, GAMEWIN, GAMECLEAR}

class Game extends Thread{
	Player p1;
	Player p2;
	Map gameMap;
	int level;
	mode gameMode;
	ArrayList<Button> btnlist;
	int turn;
	int canonNum;
	int curCanon;
	int winner;
	boolean gamesetting;					//doing thread or not
	state gamestate;
	private ArrayList<PImage> htplist;
	int htppage;
	String id;


	Game() {
		this.htplist = new ArrayList<PImage>();
		htplist.add(loadImage("howtoplay1.png"));
		htplist.add(loadImage("howtoplay2.png"));
		htplist.add(loadImage("howtoplay3.png"));
		htplist.add(loadImage("howtoplay4.png"));
		this.htppage = 0;
		this.id = "\n";
		this.initialize();
		start();
	}

	void initialize() {
		this.gamestate = state.MAIN;
		this.gamesetting = false;
		this.btnlist = new ArrayList<Button>();
		btnlist.add(new newButton());
		btnlist.add(new setButton(this.id));
		btnlist.add(new pvpButton());
		btnlist.add(new howButton());
		btnlist.add(new exitButton());
		this.curCanon = -1;
	}

	void loadMapButton() {
		File f = new File(sketchPath("data/maps.json"));
		if(!f.exists()) return;

		JSONObject json = loadJSONObject("data/maps.json");

		JSONArray list = json.getJSONArray("maps");
		if(list == null) {
			return;
		}                  

		for(int i = 0; i < list.size(); i++) {
			if(i == 10) {
				break;
			}
			JSONObject tmp = list.getJSONObject(i);
			btnlist.add(new mapButton(i,tmp));
		}
	}

	void restartGame() {
		this.p1.reinit();
		this.p2.reinit();
		this.gameMap.reinit();
		this.winner = 0;
		this.btnlist = new ArrayList<Button>();
	}

	void nextLevel(int level) {
		this.level = level;
		this.p1.reinit();
		this.p2 = pMaker.makeAI(level, p1);
		this.settingCanon();

		this.winner = 0;
		this.btnlist = new ArrayList<Button>();
	}

	void loadGame(String id, int level) {
		this.level = level;
		this.p1 = pMaker.loadfromData(id);
		this.p2 = pMaker.makeAI(level,p1);
		this.gameMode = mode.AI;
		this.settingCanon();

		this.winner = 0;
		this.btnlist = new ArrayList<Button>();
		loadMapButton();
	}

	void pvpset(JSONObject p1, JSONObject p2, int[][] gmap) {
		this.level = 1;
		this.gameMode = mode.PVP;
		this.p1 = pMaker.makeP1(p1.getString("type"));
		this.p2 = pMaker.makeP2(p2.getString("type"));
		this.settingCanon();
		this.winner = 0;
		this.btnlist = new ArrayList<Button>();
		this.gameMap = new Map(gmap);
		gameStart();
	}

	void newGame(boolean pvp) {
		this.level = 1;
		if(pvp) {
			this.p1 = pMaker.makeP1();
			this.p2 = pMaker.makeP2();
			this.gameMode = mode.PVP;
		} else {
			this.p1 = pMaker.makeP1();
			this.p2 = pMaker.makeAI(1, p1);
			this.gameMode = mode.AI;
		}
		this.settingCanon();
		
		this.winner = 0;
		this.btnlist = new ArrayList<Button>();
		loadMapButton();
	}

	void gameStart() {
		this.btnlist = new ArrayList<Button>();
		this.gamestate = state.GAMEPLAYING;
		this.gamesetting = true;
	}

	void choosingMapbyJSON(JSONObject map) {
		this.gameMap = new Map(map);
	}

	void choosingMapbyLevel(int level) {
		switch (level) {
			case 15: this.gameMap = new Map(10);
				break;
			case 14:
			case 12:
			case 10: this.gameMap = new Map(6);
				break;
			case 13:
			case 7:
			case 11: this.gameMap = new Map(5);
				break;
			case 6:
			case 8: this.gameMap = new Map(4);
				break;
			case 9:
			case 5: this.gameMap = new Map(8);
				break;
			case 4: this.gameMap = new Map(2);
				break;
			case 3: 
			case 2: this.gameMap = new Map(3);
				break;
			default : this.gameMap = new Map(1);
				break;	
		}
	}

	private void settingCanon() {
		int n = 0;
		for(Tank t : p1.tankList) {
			t.setID(n++);
		}
		for(Tank t : p2.tankList) {
			t.setID(n++);
		}
		this.canonNum = n;
	}

	void draw() {
		if(isPlaying()) {
			this.gameMap.draw();
			this.p1.draw();
			this.p2.draw();
			fill(255);
			textAlign(LEFT);
			textSize(50);
			if(this.gameMode == mode.AI) {	
				text("Level : " + Integer.toString(level),0,50);
				if(mouseX < 200 && mouseX > 0 && mouseY < 100 && mouseY > 50) {
					fill(#F5F5DC);
					rectMode(CORNER);
					rect(20, 70, 250, 150, 10);
					textSize(30);
					fill(0);
					text("Gold : " + Integer.toString(p1.getMoney()), 30,110);
					text("Power : " + Integer.toString(p1.tankList.get(0).getPower()), 30, 150);
					text("MAX HP : " + Integer.toString(p1.tankList.get(0).getHP()), 30, 190);
				} else {
					text("Status",0,100);
					stroke(255);
					strokeWeight(5);
					line(156,75,173,95);
					line(173,95,190,75);
					noStroke();
				}
			} else {
				text("PVP mode",0,50);
			}

			fill(255);
			rectMode(CORNER);
			if(mouseX < 1480 && mouseX > 1440 && mouseY < 60 && mouseY > 20) {
				rect(1444, 19, 12, 42);
				rect(1464, 19, 12, 42);
			} else {
				rect(1445, 20, 10, 40);
				rect(1465, 20, 10, 40);
			}
		}
		for(Button btn : btnlist) {
			btn.draw();
		}
		if(this.gamestate == state.HOWTOPLAY) {
			fill(255);
			textAlign(CENTER, CENTER);
			textSize(100);
			text("This is not an Octopus",width/2,height/6);
			drawHowToPlay();
		} else if(this.gamestate == state.GAMEOVER || this.gamestate == state.GAMEWIN) {
			fill(255);
			textAlign(CENTER,CENTER);
			textSize(150);
			if(this.gameMode == mode.PVP) {
				text("Player "+ Integer.toString(winner)+" win!!", 750, 300);
			} else {
				if(this.winner == 1) {
					text("Level Clear", 750, 300);
				} else {
					text("Game Over", 750, 300);
				}
				fill(#FFD400);
				textSize(40);
				text("Gold : " + Integer.toString(p1.getMoney()), 300, 490);
				fill(255);
				text("Power : " + Integer.toString(p1.tankList.get(0).getPower()), width/2 + 165, 420);
				text("HP : " + Integer.toString(p1.tankList.get(0).getHP()), width/2 - 165, 420);	
			}
		} else if(this.gamestate == state.CHOOSINGMAP) {
			fill(255);
			textAlign(CENTER,CENTER);
			textSize(50);
			text("Choosing Map",750,100);

			stroke(255);
			rectMode(CORNER);
			if(mouseX < 1480 && mouseX > 1440 && mouseY < 60 && mouseY > 20) {
				strokeWeight(7);
			} else {
				strokeWeight(5);
			}
			line(1445, 20, 1485, 60);
			line(1445, 60, 1485, 20);
			noStroke();
		} else if(this.gamestate == state.GAMECLEAR) {
			fill(255);
			textAlign(CENTER,CENTER);
			textSize(150);
			text("Congratulation!\nGame Clear",750,200);
			fill(#FFD400);
			textSize(40);
			text("Gold : " + Integer.toString(p1.getMoney()), 300, 490);
			fill(255);
			text("Power : " + Integer.toString(p1.tankList.get(0).getPower()), width/2 + 165, 420);
			text("HP : " + Integer.toString(p1.tankList.get(0).getHP()), width/2 - 165, 420);
		} else if(this.gamestate == state.PAUSEGAME) {
			fill(255);
			textAlign(CENTER,CENTER);
			textSize(200);
			text("PAUSE",750,300);
		} else if(this.gamestate == state.MAIN) {
			fill(255);
			textAlign(CENTER, CENTER);
			textSize(100);
			text("This is not an Octopus",width/2,height/6);
		}
		
	}

	void chkBtnClick() {
		if(this.gamestate == state.HOWTOPLAY) {
			if(mouseX < 400 || mouseX > 1100) this.gamestate = state.MAIN;
			if(mouseY < 50 || mouseY > 750) this.gamestate = state.MAIN;
			if(mouseX < 1090 && mouseX > 1070 && mouseY < 80 && mouseY > 60) {
				this.gamestate = state.MAIN;
			}
			if(mouseX < 430 && mouseX > 410 && mouseY < 420 && mouseY > 380) {
				if(htppage == 0) return;
				htppage--;
			}
			if(mouseX < 1090 && mouseX > 1070 && mouseY < 420 && mouseY > 380) {
				if(htppage == 3) return;
				htppage++;
			}
			return;
		}
		if(this.gamestate == state.GAMEPLAYING) {
			if(mouseX < 1480 && mouseX > 1440 && mouseY < 60 && mouseY > 20) {
				pauseGame();
			}
		}
		if(this.gamestate == state.CHOOSINGMAP) {
			if(mouseX < 1480 && mouseX > 1440 && mouseY < 60 && mouseY > 20) {
				initialize();
				this.gamestate = state.MAIN;
			}
		}
		for(Button btn : btnlist) {
			if(btn.buttonClick()) {
				if(btn instanceof newButton) {
					newGame(false);
					this.initPlayer("\n");
					choosingMapbyLevel(1);
					gameStart();
				} else if(btn instanceof setButton) {
					int lev = ((setButton)btn).getLevel();
					loadGame(this.id,lev);
					choosingMapbyLevel(lev);
					gameStart();
				} else if(btn instanceof pvpButton) {
					newGame(true);
					this.gamestate = state.CHOOSINGMAP;
				} else if(btn instanceof howButton) {
					this.htppage = 0;
					this.gamestate = state.HOWTOPLAY;
				} else if(btn instanceof exitButton) {
					exit();
				} else if(btn instanceof restartButton) {
					restartGame();
					gameStart();
				} else if(btn instanceof nextLevelButton) {
					nextLevel(this.level + 1);
					choosingMapbyLevel(this.level+1);
					gameStart();
				} else if(btn instanceof goHomeButton) {
					initialize();
				} else if(btn instanceof mapButton) {
					mapButton mtmp = (mapButton)btn;
					choosingMapbyJSON(mtmp.getMap());
					gameStart();
				} else if(btn instanceof resumeButton) {
					resumeGame();
				} else if(btn instanceof upgradeHPButton) {
					p1.upgradeTank("HP");
					savePlayer(this.id);
				} else if(btn instanceof upgradePowerButton) {
					p1.upgradeTank("Power");
					savePlayer(this.id);
				}
			}
		}
	}

	void pauseGame() {
		this.gamestate = state.PAUSEGAME;
		btnlist.add(new goHomeButton());
		btnlist.add(new restartButton());
		btnlist.add(new resumeButton());	
	}

	void resumeGame() {
		gamestate = state.GAMEPLAYING;
		btnlist = new ArrayList<Button>();
	}

	boolean isPlaying() {
		if(this.gamestate == state.GAMEPLAYING) {
			return true;
		}
		return false;
	}

	void drawHowToPlay() {
		fill(#F5F5DC);
		rectMode(CENTER);
		rect(width/2,height/2,700,700,10);
		imageMode(CENTER);
		image(htplist.get(htppage), width/2, height/2 + 10, 680, 680);
		stroke(0);
		if(mouseX < 1090 && mouseX > 1070 && mouseY < 80 && mouseY > 60) {
			strokeWeight(2);
		} else {
			strokeWeight(1);
		}
		line(1090, 80, 1070, 60);
		line(1070, 80, 1090, 60);
		if(htppage < 3) {
			if(mouseX < 1090 && mouseX > 1070 && mouseY < 420 && mouseY > 380) {
				strokeWeight(2);
			} else {
				strokeWeight(1);
			}
			line(1090, 400, 1070, 380);
			line(1070, 420, 1090, 400);
		}
		if(htppage > 0) {
			if(mouseX < 430 && mouseX > 410 && mouseY < 420 && mouseY > 380) {
				strokeWeight(2);
			} else {
				strokeWeight(1);
			}
			line(410, 400, 430, 380);
			line(430, 420, 410, 400);
		}
		noStroke();
		strokeWeight(1);
	}

	private Tank getTankbyID(int i) {
		if(p1.getTankbyID(i) == null) {
			this.turn = 2;
			return p2.getTankbyID(i);
		}
		this.turn = 1;
		return p1.getTankbyID(i);
	}

	boolean checkEndGame() {
		if(p1.checkDie()) {
			this.winner = 2;
			endGame();
			return true;
		}
		if(p2.checkDie()) {
			this.winner = 1;
			endGame();
			return true;
		}
		return false;
	}

	void initPlayer(String id) {
		this.id = id;
		File f = new File(sketchPath("data/players.json"));
		JSONObject players;
		JSONArray list;
		if(!f.exists()) {
			players = new JSONObject();
			list = new JSONArray();
		} else {
			players = loadJSONObject("data/players.json");
			list = players.getJSONArray("players");
			if(list == null) {
				list = new JSONArray();
			}
		}
		if(list.size() == 0) {
			JSONObject player = new JSONObject();
			player.setString("id",id);
			player.setInt("level",0);
			player.setInt("power",10);
			player.setInt("money",0);
			player.setInt("maxhp",100);
			player.setString("type","default");
			list.setJSONObject(0,player);
			players.setJSONArray("players",list);
			saveJSONObject(players,"data/players.json");
			return;
		}
		for(int i = 0; i < list.size(); i++) {
			JSONObject player = list.getJSONObject(i);
			if(player.getString("id").equals(id)) {
				player.setInt("level",0);
				player.setInt("power",10);
				player.setInt("money",0);
				player.setInt("maxhp",100);
				player.setString("type","default");
				break;
			}
		}
		saveJSONObject(players,"data/players.json");
	}

	void savePlayer(String id) {
		File f = new File(sketchPath("data/players.json"));
		JSONObject players;
		JSONArray list;
		if(!f.exists()) {
			players = new JSONObject();
			list = new JSONArray();
		} else {
			players = loadJSONObject("data/players.json");
			list = players.getJSONArray("players");
			if(list == null) {
				list = new JSONArray();
			}
		}
		if(list.size() == 0) {
			JSONObject player = new JSONObject();
			player.setString("id",id);
			player.setInt("level",this.level);
			player.setInt("power",this.p1.tankList.get(0).getPower());
			player.setInt("money",this.p1.getMoney());
			player.setInt("maxhp",this.p1.tankList.get(0).getHP());
			player.setString("type",this.p1.type);
			list.setJSONObject(0,player);
			players.setJSONArray("players",list);
			saveJSONObject(players,"data/players.json");
			return;
		}
		for(int i = 0; i < list.size(); i++) {
			JSONObject player = list.getJSONObject(i);
			if(player.getString("id").equals(id)) {
				if(player.getInt("level") < this.level) {
					player.setInt("level",this.level);
				}
				player.setInt("power",this.p1.tankList.get(0).getPower());
				player.setInt("money",this.p1.getMoney());
				player.setInt("maxhp",this.p1.tankList.get(0).getHP());
				break;
			}
		}
		saveJSONObject(players,"data/players.json");
		return;
	}


	void endGame() {
		
		if(this.gameMode == mode.AI && this.winner == 1) {
			if(this.level == 15) {
				this.gamestate = state.GAMECLEAR;
				p1.incMoney(1000);
				btnlist.add(new goHomeButton("boss"));
			} else {
				p1.incMoney(((int)(this.level / 5 + 1)) * 10);
				this.gamestate = state.GAMEWIN;
				btnlist.add(new nextLevelButton());
				btnlist.add(new goHomeButton());
			}
			savePlayer(this.id);
			btnlist.add(new upgradeHPButton());
			btnlist.add(new upgradePowerButton());
		} else {
			this.gamestate = state.GAMEOVER;
			if(this.gameMode == mode.AI) {
				btnlist.add(new upgradeHPButton());
				btnlist.add(new upgradePowerButton());
			}
			btnlist.add(new goHomeButton());
			btnlist.add(new restartButton());
		}
	}

	void run() {
		while(true) {
			try {
				Thread.sleep(20);
			}catch (Exception e) {}
			if(this.gamesetting) {
				int cnt = 1;
				int tmpx;
				float tmpy;
				while(cnt!=0) {
					cnt = 0;
					for(Tank tank1 : p1.tankList) {
						tmpx = tank1.getX();
						tmpy = tank1.getY();
						if(!gameMap.blockExist(tmpx,(int)(tmpy+1))) {
							tank1.setPos(tmpx,tmpy+0.025);
							cnt++;
						} else {
							int ttt = 0;
							if(gameMap.whichBlockhit(tmpx,(int)(tmpy+1)) == 4) {
								gameMap.blocks[tmpx][(int)(tmpy+1)] = -1;
								ttt = 1;
							} 
							if(gameMap.whichBlockhit(tmpx+1,(int)(tmpy+1)) == 4) {
								gameMap.blocks[tmpx+1][(int)(tmpy+1)] = -1;
								ttt = 1;
							}
							if(ttt == 1) {
								tank1.setPos(tmpx,tmpy+0.025);
								cnt++;
							}
						}
					}
					for(Tank tank2 : p2.tankList) {
						tmpx = tank2.getX();
						tmpy = tank2.getY();
						// if(!gameMap.blockExist(tmpx,(int)(tmpy + 1))) {
						// 	tank2.setPos(tmpx,tmpy+0.025);
						// 	cnt++;
						// }
						if(tank2 instanceof BossTank) {
							if(!gameMap.blockExist(tmpx,(int)(tmpy + 3))) {
								tank2.setPos(tmpx,tmpy+0.025);
								cnt++;
							}
						} else {
							if(!gameMap.blockExist(tmpx,(int)(tmpy + 1))) {
								tank2.setPos(tmpx,tmpy+0.025);
								cnt++;
							} else {
								int ttt = 0;
								if(gameMap.whichBlockhit(tmpx,(int)(tmpy+1)) == 4) {
									gameMap.blocks[tmpx][(int)(tmpy+1)] = -1;
									ttt = 1;
								} 
								if(gameMap.whichBlockhit(tmpx+1,(int)(tmpy+1)) == 4) {
									gameMap.blocks[tmpx+1][(int)(tmpy+1)] = -1;
									ttt = 1;
								}
								if(ttt == 1) {
									tank2.setPos(tmpx,tmpy+0.025);
									cnt++;
								}
							}
						}
						
					}
					try {
						Thread.sleep(20);
					}catch (Exception e) {}
				}
				for(Tank tank1 : p1.tankList) {
					tmpx = tank1.getX();
					tmpy = tank1.getY();
					tank1.setPos(tmpx,(float)((int)tmpy));
					//gameMap.setObject(tmpx,(int)tmpy);
				}
				for(Tank tank2 : p2.tankList) {
					tmpx = tank2.getX();
					tmpy = tank2.getY();
					tank2.setPos(tmpx,(float)((int)tmpy));
					//gameMap.setObject(tmpx,(int)tmpy);
				}
				this.turn = 1;
				if(gameMode == mode.AI) {
					((AIplayer)this.p2).setTarget(this.getTankbyID(0));
				}
				try {
					Thread.sleep(1000);
				}catch (Exception e) {}
				this.curCanon = 0;
				getTankbyID(0).togShooting();
				this.gamesetting = false;
			}
		}
		
	}

	void changeTurn() {
		getTankbyID(this.curCanon++).togShooting();
		if(curCanon == canonNum) {
			curCanon = 0;
		}
		Tank nTank = getTankbyID(this.curCanon);
		while(nTank.isDie()) {
			this.curCanon++;
			if(curCanon == canonNum) {
				curCanon = 0;
			}
			nTank = getTankbyID(this.curCanon);
		}

		if(nTank instanceof BossTank) {
			((BossTank)nTank).setTar(13,1000);
		} else if(nTank instanceof AITank) {
			PVector tmp = ((AIplayer)p2).computeShoot(nTank);
			((AITank)nTank).setShoot(tmp.x >= 0, tmp.y >= 500);
			((AITank)nTank).setTar(tmp.x,tmp.y);
		}
		nTank.togShooting();
	}

	boolean shouldGone(Bullet b) {
		float bx, by;
		bx = b.getX();
		by = b.getY();
		if(checkBounce(bx,by,b)) {
			return false;
		}
		int tx, ty;
		tx = (int)bx / 75;
		ty = ((int)by - 50) / 75;
		int bt = this.gameMap.whichBlockhit(tx,ty);
		if(bt == 4) {
			this.gameMap.blocks[tx][ty] = -1;
			return true;
		} else if(bt == 3) {
			if(b.stillBlock(bt)) return false;
			PVector ch = this.gameMap.getDesPortal(tx,ty);
			warp.play();
			b.warpB(ch.x,ch.y);
			return false;
		}
		b.stillBlock(bt);
		return this.gameMap.sthExist(tx,ty);
	}

	boolean checkBounce(float bx, float by, Bullet b) {
		int tx = (int)bx / 75;
		int rx = ((int)bx + 17) / 75;
		int lx = ((int)bx - 17) / 75;
		int ty = ((int)by - 50) / 75;
		int tty = ((int)by - 67) / 75;
		int bby = ((int)by - 33) / 75;
		if(this.gameMap.whichBlockhit(rx,ty) == 2 || this.gameMap.whichBlockhit(lx,ty) == 2) {
			if(b.getBounce() != 0) return true;
			bounce.play();
			b.bounceB(2);
			return true;
		}
		if(this.gameMap.whichBlockhit(tx,tty) == 2 || this.gameMap.whichBlockhit(tx,bby) == 2) {
			if(b.getBounce() != 0) return true;
			bounce.play();
			b.bounceB(1);
			return true;
		}
		b.bounceB(0);
		return false;
	}

	int checkHit(Bullet b) {
		float bx, by;
		bx = b.getX();
		by = b.getY();
		int tx, ty;
		boolean tmp = false;
		for(Tank tank : p1.tankList) {
			if(!tank.isDie()) {
				tx = tank.getX() * 75 + 75;
				ty = (int)tank.getY() * 75 + 80;
				if((by - ty) * (by - ty) + (bx - tx) * (bx - tx) < 5625) {
					tmp = true;
					b.setHitId(tank.getID());
				}
			}
		}
		for(Tank tank : p2.tankList) {
			if(!tank.isDie()) {
				tx = tank.getX() * 75 + 75;
				ty = (int)tank.getY() * 75 + 80;
				if(tank instanceof BossTank) {
					if((by - ty) * (by - ty) + (bx - tx) * (bx - tx) < 15625) {
						tmp = true;
						b.setHitId(tank.getID());
					}
				} else {
					if((by - ty) * (by - ty) + (bx - tx) * (bx - tx) < 5625) {
						tmp = true;
						b.setHitId(tank.getID());
					}
				}
			}
		}
		b.setIn(tmp);
		if(b.isHit()) {
			return b.getHitId();
		}
		return -1;
	}

	

}
