class Map {
    int w;
    int h;
    int[][] blocks;                                         // 0 : empty, 1 : block, 2 : bounce block, 3 : portal block, 4 : destroyable block
    boolean[][] objects;
    color c;
    ArrayList<Integer> portalList = new ArrayList<Integer>();

    Map() {
        initialize();
        for(int i = 0; i < w; i++) {
            for(int j = 0; j < h; j++) {
                objects[i][j] = false;
                if(h-j <= 2) {
                    blocks[i][j] = 1;
                } else {
                    blocks[i][j] = 0;
                }
            }
        }
    }

    Map(int[][] blocks) {
        initialize();
        for(int j = 0; j < h; j++) {
            for (int i = 0; i < w; i++) {
                this.blocks[i][j] = blocks[i][j];
                if(blocks[i][j] == 3) {
                    this.portalList.add(i);
                    this.portalList.add(j);
                }
            }
        }
    }

    Map(int index) {
        initialize();
        File f = new File(sketchPath("data/maps.json"));
		if(!f.exists()) return;

		JSONObject json = loadJSONObject("data/maps.json");

		JSONArray list = json.getJSONArray("maps");
		if(list == null) {
			return;
		}

		for(int i = 0; i < list.size(); i++) {
			if(index == i+1) {
                JSONObject tmp = list.getJSONObject(i);
                JSONArray values = tmp.getJSONArray("blocks");
                if(values == null) {
                    return;
                }
                for(int k = 0; k < w; k++) {
                    for(int j = 0; j < h; j++) {
                        this.blocks[k][j] = 0;
                    }
                }
                for(int k = 0; k<values.size(); k++) {
                    JSONObject tt = values.getJSONObject(k);
                    this.blocks[tt.getInt("x")][tt.getInt("y")] = 1;
                }
                break;
            }
		}
    }

    Map(JSONObject json) {
        initialize();
        JSONArray values = json.getJSONArray("blocks");
        if(values == null) {
            return;
        }
        for(int i = 0; i < w; i++) {
            for(int j = 0; j < h; j++) {
                this.blocks[i][j] = 0;
            }
        }
        for(int i = 0; i<values.size(); i++) {
            JSONObject tmp = values.getJSONObject(i);
            this.blocks[tmp.getInt("x")][tmp.getInt("y")] = 1;
        }
    }

    void initialize() {
        this.blocks = new int[20][10];
        this.objects = new boolean[20][10];
        this.w = 20;
        this.h = 10;
        this.c = #3D2324;
        this.portalList = new ArrayList<Integer>();
    }

    PVector getDesPortal(int tx, int ty) {
        for(int i = 0; i < this.portalList.size(); i+=2) {
            if(tx == portalList.get(i) && ty == portalList.get(i+1)) {
                if(i + 2 >= this.portalList.size()) {
                    return new PVector(this.portalList.get(i+2-this.portalList.size())-tx,this.portalList.get(i+3-this.portalList.size())-ty);
                } else {
                    return new PVector(this.portalList.get(i+2)-tx,this.portalList.get(i+3)-ty);
                }
            }
        }
        return new PVector(tx,ty);
    }

    void reinit() {
        for(int i = 0; i < 20; i++) {
            for(int j = 0; j < 10; j++) {
                if(this.blocks[i][j] == -1) {
                    blocks[i][j] = 4;
                }
            }
        }
    }

    void setBlock(int x, int y) {
        if(x < 0 || x >= w || y < 0 || y >= h) {
            return;
        }
        this.blocks[x][y] = 1;
    }

    boolean blockExist(int x, int y) {
        if(x >= w || x < 0 || y < 0) return false;
        if(y >= h) return true;
        return blocks[x][y] > 0 || blocks[x+1][y] > 0;
    }

    boolean sthExist(int x, int y) {
        if(x >= w || x < 0 || y >= h || y < 0) return false;
        return blocks[x][y] > 0 || objects[x][y];
    }

    int whichBlockhit(int x, int y) {
        if(x >= w || x < 0 || y >=h || y < 0) return 0;
        if(objects[x][y]) {
            return 1;
        }
        return blocks[x][y];
    }

    void setObject(int x, int y) {
        objects[x][y] = true;
        objects[x+1][y] = true;
        objects[x+1][y+1] = true;
        objects[x][y+1] = true;
    }

    void draw() {

        noStroke();
        rectMode(CORNER);
        for(int i = 0; i < w; i++) {
            for(int j = 0; j < h; j++) {
                if(blocks[i][j] > 0) {
                    if(blocks[i][j] == 1) {
                        fill(c);
                    } else if (blocks[i][j] == 2) {
                        fill(0,128,128);
                    } else if(blocks[i][j] == 3) {
                        fill(55,238,17);
                    } else if(blocks[i][j] == 4) {
                        fill(109,41,44);
                    }
                    
                    if(j == 0) {
                        rect(75*i,75*j,75,125);
                    } else {
                        rect(75*i,75*j+50,75,75);
                    }
                }
            }
        }
    }
    
}