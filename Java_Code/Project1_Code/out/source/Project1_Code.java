import processing.core.*; 
import processing.data.*; 
import processing.event.*; 
import processing.opengl.*; 

import websockets.*; 
import java.util.*; 
import processing.sound.*; 

import java.util.HashMap; 
import java.util.ArrayList; 
import java.io.File; 
import java.io.BufferedReader; 
import java.io.PrintWriter; 
import java.io.InputStream; 
import java.io.OutputStream; 
import java.io.IOException; 

public class Project1_Code extends PApplet {

/*
	Project 1
	Name of Project: This is not an Octopus
	Author: Junyoung Choi
	Date: 2020.05.31
*/






Game game;
Bullet bullet = null;
loadPlayer pMaker = new loadPlayer();
SoundFile explode;
SoundFile shot;
WebsocketServer ws;

public void setup()
{
	// your code
	
	noStroke();
	game = new Game();
	explode = new SoundFile(this,"explode.wav");
	shot = new SoundFile(this,"shoot.wav");

	ws = new WebsocketServer(this, 8025, "/test");
}

public void draw()
{
	// your code
	background(0xff000269);
	
	if(game.isPlaying()) {
		if(bullet != null) {
			if(bullet.draw()) {
				bullet = null;
				game.changeTurn();
			} else {
				int hitTankId = game.checkHit(bullet);
				int damage = bullet.getPower();
				if(hitTankId != -1) {
					bullet = null;
					explode.play();
					game.getTankbyID(hitTankId).hurt(damage);
					if(!game.checkEndGame()) {
						game.changeTurn();
					}
				} else if(game.shouldGone(bullet)) {
					bullet = null;
					explode.play();
					game.changeTurn();
				}
			}
		} else {
			Tank curCan = game.getTankbyID(game.curCanon);
			if(curCan instanceof AITank) {
				if(((AITank)curCan).canUpdate()) {
					bullet = ((AITank)curCan).getbullet();
				}
			}
		}
	}
	
	rectMode(CORNER);
	game.draw();
	
	
}


// your code down here
// feel free to crate other .pde files to organize your code

public void webSocketServerEvent(String msg)
{
	JSONObject json = parseJSONObject(msg);
	try {
		synchronized(game){
			if(json!=null) {
				println("Received: ");
				println(json);
				println("====");
				return;
			}
		}
	} catch(Exception ie) {
		println("Invalid command");
	}
}

public void mousePressed()
{
	game.chkBtnClick();
}

public void keyPressed() {
	if(game.curCanon >= 0 && game.curCanon < game.canonNum) {
		if(game.turn == 1) {
			Tank curCan = game.getTankbyID(game.curCanon);
			if(keyCode == LEFT) {
				curCan.decDist();
			} else if(keyCode == RIGHT) {
				curCan.incDist();
			} else if(keyCode == UP) {
				curCan.incAngle();
			} else if(keyCode == DOWN) {
				curCan.decAngle();
			} else if(keyCode == ENTER) {
				if(bullet == null) {
					bullet = curCan.shoot();
				}
			}
		} else if(game.gameMode == mode.PVP) {
			Tank curCan = game.getTankbyID(game.curCanon);
			if(keyCode == LEFT) {
				curCan.incDist();
			} else if(keyCode == RIGHT) {
				curCan.decDist();
			} else if(keyCode == UP) {
				curCan.incAngle();
			} else if(keyCode == DOWN) {
				curCan.decAngle();
			} else if(keyCode == ENTER) {
				if(bullet == null) {
					bullet = curCan.shoot();
				}
			}
		}	
	}
}

public enum mode {PVP, AI}
public enum state {MAIN, GAMEPLAYING, HOWTOPLAY, GAMEOVER, CHOOSINGMAP, PAUSEGAME, GAMEWIN, GAMECLEAR}

class Game extends Thread{
	Player p1;
	Player p2;
	Map gameMap;
	int level;
	mode gameMode;
	ArrayList<Button> btnlist;
	int turn;
	int canonNum;
	int curCanon;
	int winner;
	boolean gamesetting;					//doing thread or not
	state gamestate;
	private PImage htpImage;


	Game() {
		this.htpImage = loadImage("howtoplay.png");
		this.initialize();
		start();
	}

	public void initialize() {
		this.gamestate = state.MAIN;
		this.gamesetting = false;
		this.btnlist = new ArrayList<Button>();
		btnlist.add(new newButton());
		btnlist.add(new setButton(0));
		btnlist.add(new pvpButton());
		btnlist.add(new howButton());
		btnlist.add(new exitButton());
		this.curCanon = -1;
	}

	public void loadMapButton() {
		File f = new File(sketchPath("data/maps.json"));
		if(!f.exists()) return;

		JSONObject json = loadJSONObject("data/maps.json");

		JSONArray list = json.getJSONArray("maps");
		if(list == null) {
			return;
		}                  

		for(int i = 0; i < list.size(); i++) {
			if(i == 10) {
				break;
			}
			JSONObject tmp = list.getJSONObject(i);
			btnlist.add(new mapButton(i,tmp));
		}
	}

	public void restartGame() {
		this.p1.reinit();
		this.p2.reinit();
		this.winner = 0;
		this.btnlist = new ArrayList<Button>();
	}

	public void nextLevel(int level) {
		this.level = level;
		this.p1.reinit();
		this.p2 = pMaker.makeAI(level, p1);
		this.settingCanon();

		this.winner = 0;
		this.btnlist = new ArrayList<Button>();
	}

	public void loadGame(int id, int level) {
		this.level = level;
		this.p1 = pMaker.loadfromData(id);
		this.p2 = pMaker.makeAI(level,p1);
		this.gameMode = mode.AI;
		this.settingCanon();

		this.winner = 0;
		this.btnlist = new ArrayList<Button>();
		loadMapButton();
	}


	public void newGame(boolean pvp) {
		this.level = 1;
		if(pvp) {
			this.p1 = pMaker.makeP1();
			this.p2 = pMaker.makeP2();
			this.gameMode = mode.PVP;
		} else {
			this.p1 = pMaker.makeP1();
			this.p2 = pMaker.makeAI(1, p1);
			this.gameMode = mode.AI;
		}
		this.settingCanon();
		
		this.winner = 0;
		this.btnlist = new ArrayList<Button>();
		loadMapButton();
	}

	public void gameStart() {
		this.btnlist = new ArrayList<Button>();
		this.gamestate = state.GAMEPLAYING;
		this.gamesetting = true;
	}

	public void choosingMapbyJSON(JSONObject map) {
		this.gameMap = new Map(map);
	}

	public void choosingMapbyLevel(int level) {
		switch (level) {
			case 15: this.gameMap = new Map(10);
				break;
			case 14:
			case 12:
			case 10: this.gameMap = new Map(6);
				break;
			case 13:
			case 7:
			case 11: this.gameMap = new Map(5);
				break;
			case 6:
			case 8: this.gameMap = new Map(4);
				break;
			case 9:
			case 5: this.gameMap = new Map(8);
				break;
			case 4: this.gameMap = new Map(2);
				break;
			case 3: 
			case 2: this.gameMap = new Map(3);
				break;
			default : this.gameMap = new Map(1);
				break;	
		}
	}

	private void settingCanon() {
		int n = 0;
		for(Tank t : p1.tankList) {
			t.setID(n++);
		}
		for(Tank t : p2.tankList) {
			t.setID(n++);
		}
		this.canonNum = n;
	}

	public void draw() {
		if(isPlaying()) {
			this.gameMap.draw();
			this.p1.draw();
			this.p2.draw();
			fill(255);
			textAlign(LEFT);
			textSize(50);
			if(this.gameMode == mode.AI) {	
				text("Level : " + Integer.toString(level),0,50);
				if(mouseX < 200 && mouseX > 0 && mouseY < 100 && mouseY > 50) {
					fill(0xffF5F5DC);
					rectMode(CORNER);
					rect(20, 70, 250, 150, 10);
					textSize(30);
					fill(0);
					text("Gold : " + Integer.toString(p1.getMoney()), 30,110);
					text("Power : " + Integer.toString(p1.tankList.get(0).getPower()), 30, 150);
					text("MAX HP : " + Integer.toString(p1.tankList.get(0).getHP()), 30, 190);
				} else {
					text("Status",0,100);
					stroke(255);
					strokeWeight(5);
					line(156,75,173,95);
					line(173,95,190,75);
					noStroke();
				}
			} else {
				text("PVP mode",0,50);
			}

			fill(255);
			rectMode(CORNER);
			if(mouseX < 1480 && mouseX > 1440 && mouseY < 60 && mouseY > 20) {
				rect(1444, 19, 12, 42);
				rect(1464, 19, 12, 42);
			} else {
				rect(1445, 20, 10, 40);
				rect(1465, 20, 10, 40);
			}
		}
		for(Button btn : btnlist) {
			btn.draw();
		}
		if(this.gamestate == state.HOWTOPLAY) {
			fill(255);
			textAlign(CENTER, CENTER);
			textSize(100);
			text("This is not an Octopus",width/2,height/6);
			drawHowToPlay();
		} else if(this.gamestate == state.GAMEOVER || this.gamestate == state.GAMEWIN) {
			fill(255);
			textAlign(CENTER,CENTER);
			textSize(150);
			if(this.gameMode == mode.PVP) {
				text("Player "+ Integer.toString(winner)+" win!!", 750, 300);
			} else {
				if(this.winner == 1) {
					text("Level Clear", 750, 300);
				} else {
					text("Game Over", 750, 300);
				}
				fill(0xffFFD400);
				textSize(40);
				text("Gold : " + Integer.toString(p1.getMoney()), 300, 490);
				fill(255);
				text("Power : " + Integer.toString(p1.tankList.get(0).getPower()), width/2 + 165, 420);
				text("HP : " + Integer.toString(p1.tankList.get(0).getHP()), width/2 - 165, 420);	
			}
		} else if(this.gamestate == state.CHOOSINGMAP) {
			fill(255);
			textAlign(CENTER,CENTER);
			textSize(50);
			text("Choosing Map",750,100);

			stroke(255);
			rectMode(CORNER);
			if(mouseX < 1480 && mouseX > 1440 && mouseY < 60 && mouseY > 20) {
				strokeWeight(7);
			} else {
				strokeWeight(5);
			}
			line(1445, 20, 1485, 60);
			line(1445, 60, 1485, 20);
			noStroke();
		} else if(this.gamestate == state.GAMECLEAR) {
			fill(255);
			textAlign(CENTER,CENTER);
			textSize(150);
			text("Congratulation!\nGame Clear",750,200);
			fill(0xffFFD400);
			textSize(40);
			text("Gold : " + Integer.toString(p1.getMoney()), 300, 490);
			fill(255);
			text("Power : " + Integer.toString(p1.tankList.get(0).getPower()), width/2 + 165, 420);
			text("HP : " + Integer.toString(p1.tankList.get(0).getHP()), width/2 - 165, 420);
		} else if(this.gamestate == state.PAUSEGAME) {
			fill(255);
			textAlign(CENTER,CENTER);
			textSize(200);
			text("PAUSE",750,300);
		} else if(this.gamestate == state.MAIN) {
			fill(255);
			textAlign(CENTER, CENTER);
			textSize(100);
			text("This is not an Octopus",width/2,height/6);
		}
		
	}

	public void chkBtnClick() {
		if(this.gamestate == state.HOWTOPLAY) {
			if(mouseX < 400 || mouseX > 1100) this.gamestate = state.MAIN;
			if(mouseY < 50 || mouseY > 750) this.gamestate = state.MAIN;
			if(mouseX < 1090 && mouseX > 1070 && mouseY < 80 && mouseY > 60) {
				this.gamestate = state.MAIN;
			}
			return;
		}
		if(this.gamestate == state.GAMEPLAYING) {
			if(mouseX < 1480 && mouseX > 1440 && mouseY < 60 && mouseY > 20) {
				pauseGame();
			}
		}
		if(this.gamestate == state.CHOOSINGMAP) {
			if(mouseX < 1480 && mouseX > 1440 && mouseY < 60 && mouseY > 20) {
				initialize();
				this.gamestate = state.MAIN;
			}
		}
		for(Button btn : btnlist) {
			if(btn.buttonClick()) {
				if(btn instanceof newButton) {
					newGame(false);
					this.initPlayer(0);
					choosingMapbyLevel(1);
					gameStart();
				} else if(btn instanceof setButton) {
					int lev = ((setButton)btn).getLevel();
					loadGame(0,lev);
					choosingMapbyLevel(lev);
					gameStart();
				} else if(btn instanceof pvpButton) {
					newGame(true);
					this.gamestate = state.CHOOSINGMAP;
				} else if(btn instanceof howButton) {
					this.gamestate = state.HOWTOPLAY;
				} else if(btn instanceof exitButton) {
					exit();
				} else if(btn instanceof restartButton) {
					restartGame();
					gameStart();
				} else if(btn instanceof nextLevelButton) {
					nextLevel(this.level + 1);
					choosingMapbyLevel(this.level+1);
					gameStart();
				} else if(btn instanceof goHomeButton) {
					initialize();
				} else if(btn instanceof mapButton) {
					mapButton mtmp = (mapButton)btn;
					choosingMapbyJSON(mtmp.getMap());
					gameStart();
				} else if(btn instanceof resumeButton) {
					resumeGame();
				} else if(btn instanceof upgradeHPButton) {
					p1.upgradeTank("HP");
					savePlayer(0);
				} else if(btn instanceof upgradePowerButton) {
					p1.upgradeTank("Power");
					savePlayer(0);
				}
			}
		}
	}

	public void pauseGame() {
		this.gamestate = state.PAUSEGAME;
		btnlist.add(new goHomeButton());
		btnlist.add(new restartButton());
		btnlist.add(new resumeButton());	
	}

	public void resumeGame() {
		gamestate = state.GAMEPLAYING;
		btnlist = new ArrayList<Button>();
	}

	public boolean isPlaying() {
		if(this.gamestate == state.GAMEPLAYING) {
			return true;
		}
		return false;
	}

	public void drawHowToPlay() {
		fill(0xffF5F5DC);
		rectMode(CENTER);
		rect(width/2,height/2,700,700,10);
		imageMode(CENTER);
		image(htpImage, width/2, height/2 + 10, 680, 680);
		stroke(0);
		if(mouseX < 1090 && mouseX > 1070 && mouseY < 80 && mouseY > 60) {
			strokeWeight(2);
		} else {
			strokeWeight(1);
		}
		line(1090, 80, 1070, 60);
		line(1070, 80, 1090, 60);
		noStroke();
		strokeWeight(1);
	}

	private Tank getTankbyID(int i) {
		if(p1.getTankbyID(i) == null) {
			this.turn = 2;
			return p2.getTankbyID(i);
		}
		this.turn = 1;
		return p1.getTankbyID(i);
	}

	public boolean checkEndGame() {
		if(p1.checkDie()) {
			this.winner = 2;
			endGame();
			return true;
		}
		if(p2.checkDie()) {
			this.winner = 1;
			endGame();
			return true;
		}
		return false;
	}

	public void initPlayer(int id) {
		File f = new File(sketchPath("data/players.json"));
		JSONObject players;
		JSONArray list;
		if(!f.exists()) {
			players = new JSONObject();
			list = new JSONArray();
		} else {
			players = loadJSONObject("data/players.json");
			list = players.getJSONArray("players");
			if(list == null) {
				list = new JSONArray();
			}
		}
		if(list.size() == 0) {
			JSONObject player = new JSONObject();
			player.setInt("id",id);
			player.setInt("level",0);
			player.setInt("power",10);
			player.setInt("money",0);
			player.setInt("maxhp",100);
			list.setJSONObject(0,player);
			players.setJSONArray("players",list);
			saveJSONObject(players,"data/players.json");
			return;
		}
		for(int i = 0; i < list.size(); i++) {
			JSONObject player = list.getJSONObject(i);
			if(player.getInt("id") == id) {
				player.setInt("level",0);
				player.setInt("power",10);
				player.setInt("money",0);
				player.setInt("maxhp",100);
				break;
			}
		}
		saveJSONObject(players,"data/players.json");
	}

	public void savePlayer(int id) {
		File f = new File(sketchPath("data/players.json"));
		JSONObject players;
		JSONArray list;
		if(!f.exists()) {
			players = new JSONObject();
			list = new JSONArray();
		} else {
			players = loadJSONObject("data/players.json");
			list = players.getJSONArray("players");
			if(list == null) {
				list = new JSONArray();
			}
		}
		if(list.size() == 0) {
			JSONObject player = new JSONObject();
			player.setInt("id",id);
			player.setInt("level",this.level);
			player.setInt("power",this.p1.tankList.get(0).getPower());
			player.setInt("money",this.p1.getMoney());
			player.setInt("maxhp",this.p1.tankList.get(0).getHP());
			list.setJSONObject(0,player);
			players.setJSONArray("players",list);
			saveJSONObject(players,"data/players.json");
			return;
		}
		for(int i = 0; i < list.size(); i++) {
			JSONObject player = list.getJSONObject(i);
			if(player.getInt("id") == id) {
				if(player.getInt("level") < this.level) {
					player.setInt("level",this.level);
				}
				player.setInt("power",this.p1.tankList.get(0).getPower());
				player.setInt("money",this.p1.getMoney());
				player.setInt("maxhp",this.p1.tankList.get(0).getHP());
				break;
			}
		}
		saveJSONObject(players,"data/players.json"); 
	}


	public void endGame() {
		
		if(this.gameMode == mode.AI && this.winner == 1) {
			if(this.level == 15) {
				this.gamestate = state.GAMECLEAR;
				p1.incMoney(1000);
				btnlist.add(new goHomeButton("boss"));
			} else {
				p1.incMoney(((int)(this.level / 5 + 1)) * 10);
				this.gamestate = state.GAMEWIN;
				btnlist.add(new nextLevelButton());
				btnlist.add(new goHomeButton());
			}
			savePlayer(0);
			btnlist.add(new upgradeHPButton());
			btnlist.add(new upgradePowerButton());
		} else {
			this.gamestate = state.GAMEOVER;
			if(this.gameMode == mode.AI) {
				btnlist.add(new upgradeHPButton());
				btnlist.add(new upgradePowerButton());
			}
			btnlist.add(new goHomeButton());
			btnlist.add(new restartButton());
		}
	}

	public void run() {
		while(true) {
			try {
				Thread.sleep(20);
			}catch (Exception e) {}
			if(this.gamesetting) {
				int cnt = 1;
				int tmpx;
				float tmpy;
				while(cnt!=0) {
					cnt = 0;
					for(Tank tank1 : p1.tankList) {
						tmpx = tank1.getX();
						tmpy = tank1.getY();
						if(!gameMap.blockExist(tmpx,(int)(tmpy+1))) {
							tank1.setPos(tmpx,tmpy+0.025f);
							cnt++;
						}
					}
					for(Tank tank2 : p2.tankList) {
						tmpx = tank2.getX();
						tmpy = tank2.getY();
						// if(!gameMap.blockExist(tmpx,(int)(tmpy + 1))) {
						// 	tank2.setPos(tmpx,tmpy+0.025);
						// 	cnt++;
						// }
						if(tank2 instanceof BossTank) {
							if(!gameMap.blockExist(tmpx,(int)(tmpy + 3))) {
								tank2.setPos(tmpx,tmpy+0.025f);
								cnt++;
							}
						} else {
							if(!gameMap.blockExist(tmpx,(int)(tmpy + 1))) {
								tank2.setPos(tmpx,tmpy+0.025f);
								cnt++;
							}
						}
						
					}
					try {
						Thread.sleep(20);
					}catch (Exception e) {}
				}
				for(Tank tank1 : p1.tankList) {
					tmpx = tank1.getX();
					tmpy = tank1.getY();
					tank1.setPos(tmpx,(float)((int)tmpy));
					//gameMap.setObject(tmpx,(int)tmpy);
				}
				for(Tank tank2 : p2.tankList) {
					tmpx = tank2.getX();
					tmpy = tank2.getY();
					tank2.setPos(tmpx,(float)((int)tmpy));
					//gameMap.setObject(tmpx,(int)tmpy);
				}
				this.turn = 1;
				if(gameMode == mode.AI) {
					((AIplayer)this.p2).setTarget(this.getTankbyID(0));
				}
				try {
					Thread.sleep(1000);
				}catch (Exception e) {}
				this.curCanon = 0;
				getTankbyID(0).togShooting();
				this.gamesetting = false;
			}
		}
		
	}

	public void changeTurn() {
		getTankbyID(this.curCanon++).togShooting();
		if(curCanon == canonNum) {
			curCanon = 0;
		}
		Tank nTank = getTankbyID(this.curCanon);
		while(nTank.isDie()) {
			this.curCanon++;
			if(curCanon == canonNum) {
				curCanon = 0;
			}
			nTank = getTankbyID(this.curCanon);
		}

		if(nTank instanceof BossTank) {
			((BossTank)nTank).setTar(13,1000);
		} else if(nTank instanceof AITank) {
			PVector tmp = ((AIplayer)p2).computeShoot(nTank);
			((AITank)nTank).setShoot(tmp.x >= 0, tmp.y >= 500);
			((AITank)nTank).setTar(tmp.x,tmp.y);
		}
		nTank.togShooting();
	}

	public boolean shouldGone(Bullet b) {
		float bx, by;
		bx = b.getX();
		by = b.getY();
		int tx, ty;
		tx = (int)bx / 75;
		ty = ((int)by - 50) / 75;
		return this.gameMap.sthExist(tx,ty);
	}

	public int checkHit(Bullet b) {
		float bx, by;
		bx = b.getX();
		by = b.getY();
		int tx, ty;
		boolean tmp = false;
		for(Tank tank : p1.tankList) {
			if(!tank.isDie()) {
				tx = tank.getX() * 75 + 75;
				ty = (int)tank.getY() * 75 + 80;
				if((by - ty) * (by - ty) + (bx - tx) * (bx - tx) < 5625) {
					tmp = true;
					b.setHitId(tank.getID());
				}
			}
		}
		for(Tank tank : p2.tankList) {
			if(!tank.isDie()) {
				tx = tank.getX() * 75 + 75;
				ty = (int)tank.getY() * 75 + 80;
				if(tank instanceof BossTank) {
					if((by - ty) * (by - ty) + (bx - tx) * (bx - tx) < 15625) {
						tmp = true;
						b.setHitId(tank.getID());
					}
				} else {
					if((by - ty) * (by - ty) + (bx - tx) * (bx - tx) < 5625) {
						tmp = true;
						b.setHitId(tank.getID());
					}
				}
			}
		}
		b.setIn(tmp);
		if(b.isHit()) {
			return b.getHitId();
		}
		return -1;
	}

	

}
class Bullet extends Thread {
    private float y2, y1;
    private float x1;
    private float ix, iy;
    private float i;
    private PImage bulletImg;
    private int size;
    private boolean in;
    private int togcnt;                         //to check it is hit
    private int hitId;                          //which tank to hit
    private int power;

    Bullet(int power, float xpos, float ypos, float dist, float angle) {
        this.power = power;
        this.ix = xpos;
        this.iy = ypos;
        this.x1 = dist * cos(PI / 180 * angle) * (-1);
        this.y1 = dist * sin(PI / 180 * angle) * (-1);
        this.y2 = 100;
        this.i = 0;
        this.bulletImg = loadImage("bullet.png");
        this.size = 35;
        this.in = true;
        this.togcnt = 0;
        this.hitId = -1;
        start();
    }

    Bullet(int power, float xpos, float ypos, float dist, float angle,int size) {
        this.power = power;
        this.ix = xpos;
        this.iy = ypos;
        this.x1 = dist * cos(PI / 180 * angle) * (-1);
        this.y1 = dist * sin(PI / 180 * angle) * (-1);
        this.y2 = 200;
        this.i = 0;
        this.bulletImg = loadImage("bullet.png");
        this.size = size;
        this.in = true;
        this.togcnt = 0;
        this.hitId = -1;
        start();
    }

    public void run() {
        while(true) {
            try {
                Thread.sleep(10);
            }catch (Exception e) {}
            i+=0.025f;
        }
    }

    public int getHitId() {
        return this.hitId;
    }

    public void setHitId(int i) {
        this.hitId = i;
    }

    public boolean isIn() {
        return this.in;
    }

    public void setIn(boolean b) {
        if(this.isIn() != b) {
            togcnt++;
        }
        this.in = b;
    }

    public boolean isHit() {
        if(this.togcnt == 2) {
            return true;
        }
        return false;
    }

    private float getX() {
        return ix + x1*i;
    }

    private float getY() {
        return iy + y1*i + y2*i*i;
    }

    public int getPower() {
        return this.power;
    }

    public boolean draw() {
        imageMode(CENTER);
        image(this.bulletImg, getX(), getY(), size, size);
        if(getX() > 1500 || getX() < 0) {
            return true;
        }
        if((int)getY() > 800) {
            return true;
        }
        return false;
    }


}

class BulletBuilder {
    private int power;
    private float xpos;
    private float ypos;
    private float dist;
    private float angle;

    public BulletBuilder() {}

    public BulletBuilder power(int power) {
        this.power = power;
        return this;
    }

    public BulletBuilder xpos(float xpos) {
        this.xpos = xpos;
        return this;
    }

    public BulletBuilder ypos(float ypos) {
        this.ypos = ypos;
        return this;
    }

    public BulletBuilder dist(float dist) {
        this.dist = dist;
        return this;
    }

    public BulletBuilder angle(float angle) {
        this.angle = angle;
        return this;
    }

    public Bullet createBullet() {
        return new Bullet(power, xpos, ypos, dist, angle);
    }

    public Bullet createBossBullet() {
        return new Bullet(power, xpos, ypos, dist, angle, 150);
    }
}
class Button {
	protected String name;
	protected int xpos, w, h;
    protected int ypos;
	protected int c1,c2;
    protected int tSize;

	Button() {
		this.name = "";
		this.w = 800;
		this.h = 100;
		this.xpos = width/2;
        this.tSize = 50;
	}
	
	public boolean isOver()
	{
		if (mouseX < xpos-w/2 || mouseX > xpos+w/2) return false;
		if (mouseY < ypos-h/2 || mouseY > ypos+h/2) return false;
		return true;
	}

	public boolean buttonClick() {
		if(!isOver()) return false;
		return true;
	}

	public void draw() {
		rectMode(CENTER);
		if(!isOver()) {
            fill(c1);
        } else {
            fill(c2);
        }
		rect(xpos, ypos, w, h,10);
		fill(0);
		textAlign(CENTER,CENTER);
		textSize(tSize);
		text(name, xpos, ypos);
	}
}

class upgradeHPButton extends Button {
    upgradeHPButton() {
        this.xpos = width/2 - 165;
        this.ypos = 490;
        this.w = 300;
        this.h = 80;
        this.c1 = 0xffFFD400;
        this.c2 = 0xffEFC410;
        this.name = "upgrade HP : 10G";
        this.tSize = 25;
    }
}

class upgradePowerButton extends Button {
    upgradePowerButton() {
        this.xpos = width/2 + 165;
        this.ypos = 490;
        this.w = 300;
        this.h = 80;
        this.c1 = 0xffFFD400;
        this.c2 = 0xffEFC410;
        this.name = "upgrade Power : 30G";
        this.tSize = 25;
    }
}

class mapButton extends Button {
    JSONObject map;
    int index;

    mapButton(int i, JSONObject map) {
        this.map = map;
        this.index = i+1;
        this.c1 = 0xff4848C1;
        this.c2 = 0xff3838AF;
        this.tSize = 25;
        if(index < 10) {
            this.name = "Map 0" + Integer.toString(index);
        } else {
            this.name = "Map " + Integer.toString(index);
        }
        this.w = 150;
        this.h = 200;
        if(index <= 5) {
            this.xpos = 350 + (index - 1)*200;
            this.ypos = 275;
        } else {
            this.xpos = 350 + (index - 6)*200;
            this.ypos = 525;
        }
    }

    public JSONObject getMap() {
        return map;
    }

    public void draw() {
		rectMode(CENTER);
		if(!isOver()) {
            fill(c1);
        } else {
            fill(c2);
        }
		rect(xpos, ypos, w, h,10);
		
        fill(0xff3DB7CC);
        rectMode(CORNER);
        rect(xpos - 50, ypos - 30, 100, 50);

        fill(0xff872600);
        JSONArray values = this.map.getJSONArray("blocks");
        if(values != null) {
            for(int i = 0; i<values.size(); i++) {
                JSONObject tmp = values.getJSONObject(i);
                noStroke();
                rect(xpos - 50 + 5*tmp.getInt("x"), ypos - 30 + 5*tmp.getInt("y"), 5, 5);
            }
        }
        
        fill(0);
		textAlign(CENTER,CENTER);
		textSize(tSize);
		text(name, xpos, ypos + 50);
	}

}

class resumeButton extends Button{
    resumeButton() {
        this.xpos = width/2;
        this.ypos = 490;
        this.w = 630;
        this.h = 80;
        this.c1 = 0xffF4EB42;
        this.c2 = 0xffD3C731;
        this.name = "Resume";
        this.tSize = 25;
    }
}

class restartButton extends Button {
    restartButton() {
        this.xpos = width/2 - 165;
        this.ypos = 600;
        this.w = 300;
        this.h = 80;
        this.c1 = 0xff2FE02F;
        this.c2 = 0xff26C426;
        this.name = "Restart";
        this.tSize = 25;
    }
}

class nextLevelButton extends Button {
    nextLevelButton() {
        this.xpos = width/2 - 165;
        this.ypos = 600;
        this.w = 300;
        this.h = 80;
        this.c1 = 0xff2FE02F;
        this.c2 = 0xff26C426;
        this.name = "Next Level";
        this.tSize = 25;
    }
}

class goHomeButton extends Button {
    goHomeButton() {
        this.xpos = width/2 + 165;
        this.ypos = 600;
        this.w = 300;
        this.h = 80;
        this.c1 = 0xffCE2E2E;
        this.c2 = 0xffBC2222;
        this.name = "Go Home";
        this.tSize = 25;
    }

    goHomeButton(String isboss) {
        this.xpos = width / 2;
        this.ypos = 600;
        this.w = 630;
        this.h = 80;
        this.c1 = 0xffCE2E2E;
        this.c2 = 0xffBC2222;
        this.name = "Go Home";
        this.tSize = 25;
    }
}

class newButton extends Button {
    newButton() {
        super();
        this.ypos = height*2/6;
        this.c1 = 0xffF4EB42;
        this.c2 = 0xffD3C731;
        this.name = "New Game";
    }
}

class setButton extends Button {
    int setLevel;
    int maxLevel;
    setButton(int id) {
        super();
        this.ypos = height*3/6;
        this.c1 = 0xffEEBC3C;
        this.c2 = 0xffD19E2C;
        this.name = "Load Game : Lv. 1";
        this.setLevel = 1;
        this.maxLevel = 1;

        File f = new File(sketchPath("data/players.json"));
		if(!f.exists()) return;

        JSONObject players = loadJSONObject("data/players.json");

        JSONArray list = players.getJSONArray("players");
        if(list == null) {
            return;
        }

        for(int i = 0; i < list.size(); i++) {
			JSONObject player = list.getJSONObject(i);
            if(player.getInt("id") == id) {
                this.maxLevel = player.getInt("level") + 1;
                if(this.maxLevel > 15) this.maxLevel = 15;
                return;
            }
		}
        return;
    }

    public boolean canAdd() {
        if((mouseX > xpos + w/2 + 15) && (mouseY - ypos) / (h/2 - 7) > (mouseX - xpos - w/2 - 10 - h * 1.7f/2) / (h * 1.7f / 2 - 5) && (mouseY - ypos) / (7 - h/2) > (mouseX - xpos - w/2 - 10 - h * 1.7f/2) / (h * 1.7f / 2 - 5)) return true;
        return false;
    }

    public boolean canSub() {
        if((mouseX < xpos - w/2 - 15) && (mouseY - ypos) / (h/2 - 7) < (mouseX - xpos + w/2 + 10 + h * 1.7f/2) / (h * 1.7f / 2 - 5) && (mouseY - ypos) / (7 - h/2) < (mouseX - xpos + w/2 + 10 + h * 1.7f/2) / (h * 1.7f / 2 - 5)) return true;
        return false;
    }

    public boolean buttonClick() {
        if(canAdd()) {
            if(setLevel < this.maxLevel) {
                setLevel++;
                this.name = "Load Game : Lv. " + Integer.toString(setLevel);
            }   
        }
        if(canSub()) {
            if(setLevel > 1) {
                setLevel--;
                this.name = "Load Game : Lv. " + Integer.toString(setLevel);
            }
        }
		if(!isOver()) return false;
		return true;
	}


    public int getLevel() {
        return setLevel;
    }

    public void draw() {
		rectMode(CENTER);
		if(!isOver()) {
            fill(c1);
        } else {
            fill(c2);
        }
		rect(xpos, ypos, w, h,10);
		fill(0);
		textAlign(CENTER,CENTER);
		textSize(50);
		text(name, xpos, ypos);
        if(canAdd()) {
            fill(0xffEFEF7F);
        } else {
            fill(0xffFFFF8F);
        }
        triangle(xpos + w/2 + 15, ypos + h/2 - 7, xpos + w/2 + 15, ypos - h/2 + 7, xpos + w/2 + 10 + h * 1.7f/2, ypos);
        if(canSub()) {
            fill(0xffEFEF7F);
        } else {
            fill(0xffFFFF8F);
        }
        triangle(xpos - w/2 - 15, ypos + h/2 - 7, xpos - w/2 - 15, ypos - h/2 + 7, xpos - w/2 - 10 - h * 1.7f/2, ypos);
	}
}

class pvpButton extends Button {
    pvpButton() {
        super();
        this.ypos = height*4/6;
        this.c1 = 0xffE87E36;
        this.c2 = 0xffCE6721;
        this.name = "PVP";
    }
}

class howButton extends Button {
    howButton() {
        super();
        this.xpos = width/2 - 210;
        this.ypos = height*5/6;
        this.w = 380;
        this.c1 = 0xff2FE02F;
        this.c2 = 0xff26C426;
        this.name = "How to Play";
    }
}

class exitButton extends Button {
    exitButton() {
        super();
        this.xpos = width/2 + 210;
        this.ypos = height*5/6;
        this.w = 380;
        this.c1 = 0xffCE2E2E;
        this.c2 = 0xffBC2222;
        this.name = "Exit";
    }
}
class Map {
    int w;
    int h;
    int[][] blocks;                                         // 0 : empty, 1 : block, 2 : bounce block, 3 : portal block, 4 : destroyable block
    boolean[][] objects;
    int c;

    Map() {
        initialize();
        for(int i = 0; i < w; i++) {
            for(int j = 0; j < h; j++) {
                objects[i][j] = false;
                if(h-j <= 2) {
                    blocks[i][j] = 1;
                } else {
                    blocks[i][j] = 0;
                }
            }
        }
    }

    Map(int[][] blocks) {
        initialize();
        for(int i = 0; i < w; i++) {
            for (int j = 0; j < h; j++) {
                this.blocks[i][j] = blocks[i][j];
            }
        }
    }

    Map(int index) {
        initialize();
        File f = new File(sketchPath("data/maps.json"));
		if(!f.exists()) return;

		JSONObject json = loadJSONObject("data/maps.json");

		JSONArray list = json.getJSONArray("maps");
		if(list == null) {
			return;
		}

		for(int i = 0; i < list.size(); i++) {
			if(index == i+1) {
                JSONObject tmp = list.getJSONObject(i);
                JSONArray values = tmp.getJSONArray("blocks");
                if(values == null) {
                    return;
                }
                for(int k = 0; k < w; k++) {
                    for(int j = 0; j < h; j++) {
                        this.blocks[k][j] = 0;
                    }
                }
                for(int k = 0; k<values.size(); k++) {
                    JSONObject tt = values.getJSONObject(k);
                    this.blocks[tt.getInt("x")][tt.getInt("y")] = 1;
                }
                break;
            }
		}
    }

    Map(JSONObject json) {
        initialize();
        JSONArray values = json.getJSONArray("blocks");
        if(values == null) {
            return;
        }
        for(int i = 0; i < w; i++) {
            for(int j = 0; j < h; j++) {
                this.blocks[i][j] = 0;
            }
        }
        for(int i = 0; i<values.size(); i++) {
            JSONObject tmp = values.getJSONObject(i);
            this.blocks[tmp.getInt("x")][tmp.getInt("y")] = 1;
        }
    }

    public void initialize() {
        this.blocks = new int[20][10];
        this.objects = new boolean[20][10];
        this.w = 20;
        this.h = 10;
        this.c = 0xff3D2324;
    }

    public void setBlock(int x, int y) {
        if(x < 0 || x >= w || y < 0 || y >= h) {
            return;
        }
        this.blocks[x][y] = 1;
    }

    public boolean blockExist(int x, int y) {
        if(x >= w || x < 0 || y < 0) return false;
        if(y >= h) return true;
        return blocks[x][y] != 0 || blocks[x+1][y] != 0;
    }

    public boolean sthExist(int x, int y) {
        if(x >= w || x < 0 || y >= h || y < 0) return false;
        return blocks[x][y] != 0 || objects[x][y];
    }

    public int whichBlockhit(int x, int y) {
        if(x >= w || x < 0 || y >=h || y < 0) return 0;
        if(objects[x][y]) {
            return 1;
        }
        return blocks[x][y];
    }

    public void setObject(int x, int y) {
        objects[x][y] = true;
        objects[x+1][y] = true;
        objects[x+1][y+1] = true;
        objects[x][y+1] = true;
    }

    public void draw() {

        noStroke();
        rectMode(CORNER);
        for(int i = 0; i < w; i++) {
            for(int j = 0; j < h; j++) {
                if(blocks[i][j] != 0) {
                    if(blocks[i][j] == 1) {
                        fill(c);
                    } else if (blocks[i][j] == 2) {
                        fill(0,128,128);
                    } else if(blocks[i][j] == 3) {
                        fill(55,238,17);
                    } else if(blocks[i][j] == 4) {
                        fill(109,41,44);
                    }
                    
                    if(j == 0) {
                        rect(75*i,75*j,75,125);
                    } else {
                        rect(75*i,75*j+50,75,75);
                    }
                }
            }
        }
    }
    
}
class Player {
    int num;
	int canonAmount;
	ArrayList<Tank> tankList;
	int money;
    int level;

    Player() {
        this.canonAmount = 1;
		this.tankList = new ArrayList<Tank>();
		tankList.add(new Tank());
		this.money = 0;
        this.level = 0;
    }


	Player(int money,int level) {
        this.canonAmount = 1;
		this.tankList = new ArrayList<Tank>();
		this.money = money;
        this.setP(1);
        this.level = level;
	}

    public void draw() {
        for(Tank tank : tankList) {
            tank.draw();
        }
    }

    public void setP(int num) {
        this.num = num;
        if(num == 1) {
            for(Tank tmp : this.tankList) {
                tmp.setP1Tank();
                tmp.setPos(1,0);
            }
        } else {
            for(Tank tmp : this.tankList) {
                tmp.setP2Tank();
                tmp.setPos(17,0);
            }
        }
    }

    public void reinit() {
        for(Tank tank : tankList) {
            tank.reinit();
        }
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public int getLevel() {
        return this.level;
    }

    public void incMoney(int deg) {
        this.money += deg;
    }

    public int getMoney() {
        return this.money;
    }

    public void upgradeTank(String type) {
        if(type.equals("HP")) {          
            if(this.money < 10) {
                return;
            }
            this.money -= 10;
            this.tankList.get(0).upgradeHP(10);
        } else if(type.equals("Power")) {
            if(this.money < 30) {
                return;
            }
            this.money -= 30;    
            this.tankList.get(0).upgradePower(5);
        }
    }

    public Tank getTankbyID(int i) {
        for(Tank t : tankList) {
            if(t.getID() == i) {
                return t;
            }
        }
        return null;
    }

    public boolean checkDie() {
        int cnt = 0;
        for(Tank t : tankList) {
            if(!t.isDie()) {
                return false;
            }
        }
        return true;
    }


}

class AIplayer extends Player {
    int accuracy;
    int targetX, targetY;


    AIplayer() {
        super();
        this.tankList.remove(0);
        this.level = 1;
        this.accuracy = 100;
        this.setP(2);
    }

    public void setLevel(int level) {
        this.level = level;
        this.accuracy = 25 + level * 5;
        this.canonAmount = level / 5 + 1;
        while(this.tankList.size() < this.canonAmount) {
            AITank newTank = new AITank(10, 100, 150);
            newTank.setP2Tank();
            if(level > 10) {
                float tmp = random(0,10);
                if(tmp > 8) {
                    newTank.upgradePower(10);
                }
                if(tmp > 6) {
                    newTank.upgradeHP(50);
                }
            }
            this.tankList.add(newTank);
        }
        for(int i = 0; i < this.tankList.size(); i++) {
            tankList.get(i).setPos(17 - 3 * i, 0);
        }
    }

    public void setTarget(Tank target) {
        this.targetX = target.getX() * 75 + 75;
        this.targetY = (int)target.getY() * 75 + 80;
    }

    public PVector computeShoot(Tank t) {
        float a;
        float d;
        int ix = t.getX() * 75 + 75;
        int iy = (int)t.getY() * 75 + 80;
        float x1, y1;
        float y2 = 100;
        float i;
        float yval;
        for(d = 250; d < 1000; d++) {
            for(a = 20; a < 90; a++) {
                x1 = d * cos(PI / 180 * a) * (-1);
                y1 = d * sin(PI / 180 * a) * (-1);
                i = (this.targetX - ix) / x1;
                yval = iy + y1*i + y2*i*i;
                if(this.targetY - yval < 75 && this.targetY - yval > -75) {
                    return new PVector(a + random(-100 + this.accuracy,100 - this.accuracy)/2,d + random(-100 + this.accuracy,100 - this.accuracy));
                }
            }
        }
        return new PVector(30,500);
    }
}

class loadPlayer {

    public Player makeP1() {
        Player res = new Player();
        res.setP(1);
        return res;
    }

    public Player makeP2() {
        Player res = new Player();
        res.setP(2);
        return res;
    }

    public Player loadfromData(int id) {
        File f = new File(sketchPath("data/players.json"));
		if(!f.exists()) return null;

        JSONObject players = loadJSONObject("data/players.json");

        JSONArray list = players.getJSONArray("players");
        if(list == null) {
            return null;
        }

        for(int i = 0; i < list.size(); i++) {
			JSONObject player = list.getJSONObject(i);
            if(player.getInt("id") == id) {
                Player p = new Player(player.getInt("money"),player.getInt("level"));
                Tank t = new Tank(player.getInt("power"),player.getInt("maxhp"),150);
                p.tankList.add(t);
                p.setP(1);
                return p;
            }
		}
        return null;
    }

    public AIplayer makeAI(int level, Player tarP) {
        if(level == 15) {
            return this.makeBoss(tarP);
        }
        AIplayer res = new AIplayer();
        res.setLevel(level);
        res.setTarget(tarP.tankList.get(0));
        return res;
    }

    public AIplayer makeBoss(Player tarP) {
        AIplayer res = new AIplayer();
        res.level = 15;
        BossTank tmp = new BossTank();
        tmp.setP2Tank();
        tmp.setPos(14,0);
        res.tankList.add(tmp);
        res.setTarget(tarP.tankList.get(0));
        return res;
    }
}

class Tank{
	protected int power;                                  //power of the bullet
    protected int hp;                                     //hp if goes to 0 die
    protected int maxhp;
    protected PImage canonImg;                            //img
    protected PImage wheelImg;
    protected int xpos;                             //to draw
    int size;
    protected float ypos;
    protected int dir;                                    //heading right : -1 or left : 1
    protected boolean shooting;                           //if it is in shooting procedure
    protected boolean shootcomplete;
    protected int canonID;                                //to check turn
    
    protected float angle;                                //for shooting bullet
    protected float distance;
    protected boolean alive;

	Tank() {
		this.power = 10;
        this.hp = 100;
        this.maxhp = 100;
        this.canonImg = loadImage("canonp1.png");
        this.wheelImg = loadImage("wheelp1.png");
        this.xpos = 0;                                    //xpos ypos size should be changed
        this.ypos = 0;
        this.size = 150;
        this.dir = 1;
        this.shooting = false;
        this.shootcomplete = false;
        this.angle = 0;
        this.distance = 500;
        this.alive = true;
	}

    Tank(int power,int maxhp, int size) {
        this.power = power;
        this.hp = maxhp;
        this.maxhp = maxhp;
        this.canonImg = loadImage("canonp1.png");
        this.wheelImg = loadImage("wheelp1.png");
        this.xpos = 0;
        this.ypos = 0;
        this.size = size;
        this.dir = 1;
        this.shooting = false;
        this.shootcomplete = false;
        this.angle = 0;
        this.distance = 500;
        this.alive = true;
    }

    public void reinit() {
        this.shooting = false;
        this.shootcomplete = false;
        this.angle = 0;
        this.distance = 500;
        this.hp = this.maxhp;
        this.ypos = 0;
        this.alive = true;
    }

    public void upgradePower(int deg) {
        this.power+=deg;
    }

    public int getPower() {
        return this.power;
    }

    public void upgradeHP(int deg) {
        this.maxhp+=deg;
        this.hp+=deg;
    }

    public int getHP() {
        return this.maxhp;
    }

    public void setID(int i) {
        this.canonID = i;
    }

    public int getID() {
        return this.canonID;
    }

    public void togShooting() {
        if(this.shooting) {
            this.angle = 0;
            this.distance = 500;
        }
        this.shooting = !this.shooting;
        this.shootcomplete = false;
    }

    public float getAngle() {
        return this.angle;
    }

    public float getDist() {
        return this.distance;
    }

    public void incAngle() {
        if(this.angle >= 90) {
            return;
        }
        this.angle++;
    }

    public void decAngle() {
        if(this.angle < -45) {
            return;
        }
        this.angle--;
    }

    public void incDist() {
        this.distance++;
    }

    public void decDist() {
        if(this.distance <= 300) {
            return;
        }
        this.distance--;
    }

    public void setPos(int x, float y) {
        if(x < 0 || x > 18) {
            return;
        }
        this.xpos = x;
        this.ypos = y;
    }

    public void hurt(int x) {
        this.hp-=x;
        if(this.hp <= 0) {
            this.alive = false;
        }
    }

    public boolean isDie() {
        return !this.alive;
    }

    public int getX() {
        return this.xpos;
    }

    public float getY() {
        return this.ypos;
    }

    public Bullet shoot() {
        if(this.shootcomplete) {
            return null;
        }
        this.shootcomplete = true;
        shot.play();
        BulletBuilder bb = new BulletBuilder();
        return bb.power(this.power).xpos((float)xpos*75 + 75).ypos(ypos*75+80).dist(distance*dir).angle(angle*dir).createBullet();
    }

    public void draw() {
        if(this.alive) {

            imageMode(CENTER);

            pushMatrix();
            translate(xpos*75 + 75, ypos*75+80);
            rotate(PI / 180 * angle * dir);
            if(this.shooting) {
                stroke(0xffFF0000);
                strokeWeight(3);
                line(0, 0, (-1) * dir * distance / 4, 0);
                line((-1) * dir * distance / 4, 0, (-1) * dir * distance / 4 - (-1) * dir * 10 * distance / 500, -10 * distance / 500);
                line((-1) * dir * distance / 4, 0, (-1) * dir * distance / 4 - (-1) * dir * 10 * distance / 500, 10 * distance / 500);
                noStroke();
                strokeWeight(1);
            }
            image(this.canonImg, 0, 0, size, size);
            popMatrix();

            image(this.wheelImg, xpos*75 + 75, ypos*75+80, size, size);

            if(this.shooting) {
                fill(0xffFF0000);
                triangle(xpos*75 + 77, ypos*75 + 35, xpos*75 + 57, ypos*75 + 1, xpos*75 + 97, ypos*75 + 1);
            }
            
            rectMode(CORNER);

            fill(0xffF0F0F0);
            rect(xpos*75 + 40, ypos*75 + 140,size / 2,5);
            fill(0xff00FF00);
            rect(xpos*75 + 40, ypos*75 + 140,size / 2 * hp / maxhp,5);

        }

    }

    public void setP1Tank() {
        this.canonImg = loadImage("canonp1.png");
        this.wheelImg = loadImage("wheelp1.png");
        this.dir = -1;
    }

    public void setP2Tank() {
        this.canonImg = loadImage("canonp2.png");
        this.wheelImg = loadImage("wheelp2.png");
        this.dir = 1;
    }
}

class AITank extends Tank implements Runnable{
    float ta, td;
    Bullet bullet;
    boolean up, far;
    boolean updateBullet;

    AITank() {
        super();

        this.bullet = null;
        this.up = true;
        this.far = true;
        this.updateBullet = false;
        Thread t = new Thread(this);
        t.start();
	}

    AITank(int power, int maxhp, int size) {
        super(power, maxhp, size);
        this.bullet = null;
        this.up = true;
        this.far = true;
        this.updateBullet = false;
        Thread t = new Thread(this);
        t.start();
    }

    public void setShoot(boolean up, boolean far) {
        this.up = up;
        this.far = far;
    }

    public void setTar(float ta, float td) {
        this.ta = ta;
        this.td = td;
    }

    public boolean canUpdate() {
        return !this.updateBullet;
    }

    public Bullet getbullet() {
        if(this.bullet != null) {
            this.updateBullet = true;
        }
        return bullet;
    }

    public void run() {
        while(true) {
            try {
                Thread.sleep(50);
            }catch (Exception e) {}
            if(this.shooting && !this.shootcomplete) {
                if(this.up) {
                    if(this.ta > this.angle) {
                        this.incAngle();
                    } else if(this.far && this.td > this.distance) {
                        this.incDist();
                    } else if(!this.far && this.td < this.distance) {
                        this.decDist();
                    } else {
                        this.bullet = this.shoot();
                    }
                } else {
                    if(this.ta < this.angle) {
                        this.decAngle();
                    } else if(this.far && this.td > this.distance) {
                        this.incDist();
                    } else if(!this.far && this.td < this.distance) {
                        this.decDist();
                    } else {
                        this.bullet = this.shoot();
                    }
                }
            }
        }
    }

    public void draw() {

        if(this.alive) {

            imageMode(CENTER);
            pushMatrix();
            translate(xpos*75 + 75, ypos*75+80);
            rotate(PI / 180 * angle * dir);
            if(this.shooting) {
                stroke(0xffFF0000);
                strokeWeight(3);
                line(0, 0, (-1) * dir * distance / 4, 0);
                line((-1) * dir * distance / 4, 0, (-1) * dir * distance / 4 - (-1) * dir * 10 * distance / 500, -10 * distance / 500);
                line((-1) * dir * distance / 4, 0, (-1) * dir * distance / 4 - (-1) * dir * 10 * distance / 500, 10 * distance / 500);
                noStroke();
                strokeWeight(1);
            } else {
                this.bullet = null;
                this.updateBullet = false;
            }
            image(this.canonImg, 0, 0, size, size);
            popMatrix();

            image(this.wheelImg, xpos*75 + 75, ypos*75+80, size, size);

            if(this.shooting) {
                fill(0xffFF0000);
                triangle(xpos*75 + 77, ypos*75 + 35, xpos*75 + 57, ypos*75 + 1, xpos*75 + 97, ypos*75 + 1);
            }
            
            rectMode(CORNER);
            fill(0xffF0F0F0);
            rect(xpos*75 + 40, ypos*75 + 140,size / 2,5);
            fill(0xff00FF00);
            rect(xpos*75 + 40, ypos*75 + 140,size / 2 * hp / maxhp,5);
        
        }
    }
}


class BossTank extends AITank {

    BossTank() {
        super(1000,1000,750);
    }

    public Bullet shoot() {
        if(this.shootcomplete) {
            return null;
        }
        this.shootcomplete = true;
        shot.play();
        BulletBuilder bb = new BulletBuilder();
        return bb.power(this.power).xpos((float)xpos*75 + 75).ypos(ypos*75+80).dist(distance*dir).angle(angle*dir).createBossBullet();
    }

    public void incDist() {
        this.distance+=25;
    }


    public void draw() {
        if(this.alive) {

            imageMode(CENTER);

            pushMatrix();
            translate(xpos*75 + 75, ypos*75+80);
            rotate(PI / 180 * angle * dir);
            if(!this.shooting) {
                this.bullet = null;
                this.updateBullet = false;
            }
            image(this.canonImg, 0, 0, size, size);
            popMatrix();

            image(this.wheelImg, xpos*75 + 75, ypos*75+80, size, size);

            if(this.shooting) {
                fill(0xffFF0000);
                triangle(xpos*75 + 80, ypos*75 - 111, xpos*75 + 60, ypos*75 - 145, xpos*75 + 100, ypos*75 - 145);
            }
            
            rectMode(CORNER);
            fill(0xffF0F0F0);
            rect(xpos*75 - 160, ypos*75 + 340,size / 1.5f,5);
            fill(0xff00FF00);
            rect(xpos*75 - 160, ypos*75 + 340,size / 1.5f * hp / 1000,5);
        }
    }
}
  public void settings() { 	size(1500,800); }
  static public void main(String[] passedArgs) {
    String[] appletArgs = new String[] { "Project1_Code" };
    if (passedArgs != null) {
      PApplet.main(concat(appletArgs, passedArgs));
    } else {
      PApplet.main(appletArgs);
    }
  }
}
